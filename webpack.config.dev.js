const makeConfig = require('./make-webpack-config');
const port = process.env.APP_PORT;

module.exports = makeConfig({
    entry: [
        `webpack-dev-server/client?http://localhost:${port}`
    ]
});
