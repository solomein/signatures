import React, { Component, PropTypes } from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import { TextField } from 'dumb/text-field';
import Card from 'dumb/card';
import Dropdown from 'dumb/dropdown';
import translate from 'i18n/translate';
import css from './enter-form.css';
import { isEmail } from 'utils/string-test';

const { func, string, object, array, bool } = PropTypes;

@translate('enter')
export default class EnterForm extends Component {
    static propTypes = {
        onSubmit: func.isRequired,
        email: string,
        translation: object,
        defaultLocale: string,
        languages: array,
        onChangeLocale: func,
        disabled: bool
    };

    constructor(props) {
        super(props);
        this.state = {
            email: props.email,
            locale: props.defaultLocale || props.languages[0].locale,
            emailIsValid: isEmail(props.email)
        };
    }

    render() {
        const {
            welcome,
            email,
            language,
            enterButton
        } = this.props.translation.enter;


        return <Card>
            <div className={css.form}>
                <div className={css.logo}></div>
                <div className={css.welcome}>
                    {welcome}
                </div>
                <div className={css.input}>
                    <TextField
                        value={this.state.email}
                        placeholder={email}
                        onChange={val => this.handleEmailChange(val)}
                        onKeyPress={ev => this.handleKeyPress(ev)}
                        invalid={!this.state.emailIsValid}
                        disabled={this.props.disabled}
                    />
                </div>
                <div className={css.input}>
                    <Dropdown
                        dropdownStyle={{ width: '100%' }}
                        value={this.state.locale}
                        data={this.props.languages}
                        label={language}
                        disabled={this.props.disabled}
                        dataConverter={data => {
                            return {
                                text: data.name,
                                value: data.locale
                            };
                        }}
                        onChange={val => this.handleChangeLocale(val)}
                    />
                </div>
                <div className={css.button}>
                    <RaisedButton
                        label={enterButton}
                        primary={true}
                        onTouchTap={() => this.handleSubmit()}
                        disabled={!this.state.emailIsValid || this.props.disabled}
                    />
                </div>
            </div>
        </Card>;
    }

    handleEmailChange(email) {
        this.setState({ email, emailIsValid: isEmail(email) });
    }

    handleChangeLocale(locale) {
        this.setState({ locale });
        this.props.onChangeLocale(locale);
    }

    handleKeyPress(ev) {
        if (ev.key === 'Enter') {
            this.handleSubmit();
        }
    }

    handleSubmit() {
        this.props.onSubmit(this.state.email, this.state.locale);
    }
}
