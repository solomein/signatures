import React, { Component, PropTypes } from 'react';
import Card from 'dumb/card';
import css from './tile.css';

const { string, func, number } = PropTypes;

export default class Tile extends Component {
    static propTypes = {
        title: string,
        iconClass: func,
        count: number,
        onClick: func
    };

    render() {
        return (
            <div className={css.root} onClick={() => this.props.onClick()}>
                <Card>
                    <div className={css.content}>
                        <div>
                            <div>
                                {React.createElement(this.props.iconClass, {
                                    style: {width: 64, height: 64},
                                    color: 'rgba(0, 56, 89, 0.16)'
                                })}
                            </div>
                            {this.renderBadge()}
                        </div>
                        <div className={css.text}>{this.props.title}</div>
                    </div>
                </Card>
            </div>
       );
    }

    renderBadge() {
        const { count } = this.props;
        if (!count || count === '0') {
            return;
        }

        return <div className={css.count}>{count}</div>;
    }
}
