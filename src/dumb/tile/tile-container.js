import React, { Component } from 'react';
import css from './tile-container.css';

export default class TileContainer extends Component {
    render() {
        return (
            <div className={css.root}>{this.props.children}</div>
        );
    }
}
