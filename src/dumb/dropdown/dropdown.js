import React, { Component, PropTypes } from 'react';
import SelectedField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import { ControlLabel } from 'dumb/control-label';
import css from './dropdown.css';

const { func, string, array, number, oneOfType, bool, object } = PropTypes;

export default class Dropdown extends Component {
    static propTypes = {
        onChange: func,
        value: oneOfType([string, number]),
        data: oneOfType([array, object]),
        dataConverter: func,
        label: string,
        maxHeight: number,
        ensureValue: bool,
        dropdownStyle: object
    };

    static defaultProps = {
        dataConverter: data => data
    };

    componentWillMount() {
        if (this.props.ensureValue) {
            const data = this.props.dataConverter(this.props.data.get(0));
            this.props.onChange(this.props.value ? this.props.value : data.value);
        }
    }

    render() {
        const style = Object.assign({
            width: 'auto',
            position: 'relative',
            top: -2,
            height: 50
        }, this.props.dropdownStyle);

        return <section>
            <ControlLabel>{this.props.label}</ControlLabel>
            <div className={css.control}>
                <SelectedField
                    style={style}
                    maxHeight={this.props.maxHeight}
                    value={this.props.value}
                    onChange={(ev, i, val) => this.handleChange(val)}>
                    {
                        this.props.data.map(item => {
                            const data = this.props.dataConverter(item);
                            return <MenuItem
                                key={data.value}
                                value={data.value}
                                primaryText={data.text}
                            />;
                        })
                    }
                </SelectedField>
            </div>
        </section>;
    }

    handleChange(val) {
        this.props.onChange(val);
    }
}
