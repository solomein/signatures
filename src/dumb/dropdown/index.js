export default from './dropdown';

export function getTranslation(items, translation) {
    return items.map(item => {
        return {
            value: item,
            text: translation[item]
        }
    });
}
