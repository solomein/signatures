import React, { Component, PropTypes } from 'react';
import css from './floating-button.css';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import AddIcon from 'material-ui/svg-icons/content/add';

export default class FloatingButton extends Component {
    render() {
        return <div className={css.root}>
            <FloatingActionButton onTouchTap={() =>this.props.onTouchTap()}>
                <AddIcon />
            </FloatingActionButton>
        </div>;
    }
}
