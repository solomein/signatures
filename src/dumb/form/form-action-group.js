import React, { Component } from 'react';
import css from './form-action-group.css';

export default class FormActionGroup extends Component {
    render() {
        return (
            <section className={css.root}>
                {this.props.children}
            </section>
        );
    }
}
