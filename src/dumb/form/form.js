import React, { Component, PropTypes } from 'react';
import css from './form.css';
import { withMods } from 'utils/cssm';

const { node, string, bool } = PropTypes;
const mod = withMods(css);

export default class Form extends Component {
    static propTypes = {
        children: node,
    };

    render() {
        return (
            <section>
                {this.renderContent()}
            </section>
        );
    }

    renderContent() {
        return <div className={mod.content()}>
            {this.props.children}
        </div>
    }
}
