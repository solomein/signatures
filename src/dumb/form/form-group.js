import React, { Component } from 'react';
import css from './form-group.css';

export default class FormGroup extends Component {
    render() {
        return (
            <section className={css.root}>
                {this.props.children}
            </section>
        );
    }
}
