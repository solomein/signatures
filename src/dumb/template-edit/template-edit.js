import React from 'react';
import { Form, FormGroup } from 'dumb/form';
import { TextField } from 'dumb/text-field';
import EditForm from 'dumb/edit-form';
import Dropdown, { getTranslation } from 'dumb/dropdown';
import translate from 'i18n/translate';
import { getFieldName, getTemplateName } from 'i18n/helpers';
import Toggle from 'dumb/Toggle';
import FileUploader from 'dumb/file-uploader';
import Checkbox from 'material-ui/Checkbox';
import { ControlLabel } from 'dumb/control-label';
import Divider from 'material-ui/Divider';

@translate('entityEdit', 'adminTemplateEdit', 'templateLanguage')
export default class TemplateEdit extends EditForm {
    constructor(props) {
        super(props);

        // IE HACK
        if (!this.state) {
            EditForm.call(this, props);
        }
    }

    validateConfig() {
        return [
            {
                dataField: 'frenchName',
                stateField: 'frenchNameIsValid'
            },
            {
                dataField: 'englishName',
                stateField: 'englishNameIsValid'
            },
            {
                dataField: 'package',
                stateField: 'packageIsValid'
            },
            {
                dataField: 'thumb',
                stateField: 'thumbnailIsValid'
            }
        ];
    }

    renderMainForm(d) {
        const {
            frenchName,
            frenchNamePlaceholder,
            englishName,
            englishNamePlaceholder,
            isActive,
            language,
            templatePackage,
            thumbnail,
            fields,
            replacedBy
        } = this.props.translation.adminTemplateEdit;

        const langs = getTranslation(
            this.props.languages,
            this.props.translation.templateLanguage
        );

        return (
            <Form>
                <FormGroup>
                    <TextField
                        label={frenchName}
                        placeholder={frenchNamePlaceholder}
                        value={d.get('frenchName')}
                        onChange={val => this.handlePropChange('frenchName', val)}
                        attention={!this.state.frenchNameIsValid}
                    />
                </FormGroup>
                <FormGroup>
                    <TextField
                        label={englishName}
                        placeholder={englishNamePlaceholder}
                        value={d.get('englishName')}
                        onChange={val => this.handlePropChange('englishName', val)}
                        attention={!this.state.englishNameIsValid}
                    />
                </FormGroup>
                <FormGroup>
                    <Dropdown
                        ensureValue={true}
                        value={d.get('lang')}
                        data={langs}
                        label={language}
                        onChange={val => {this.handlePropChange('lang', val)}}
                    />
                </FormGroup>
                <Divider />
                <FormGroup>
                    <Toggle
                        defaultToggled={d.get('isActive')}
                        label={isActive}
                        onTouchTap={() => this.handlePropChange('isActive', !d.get('isActive'))}
                    />
                </FormGroup>
                <FormGroup>
                    <Dropdown
                        dropdownStyle={{width: '100%'}}
                        //ensureValue={true}
                        value={d.get('replacedById')}
                        data={this.props.templates}
                        label={replacedBy}
                        dataConverter={item => {
                            return {
                                value: item.get('id'),
                                text: getTemplateName(this.props.locale, item)
                            };
                        }}
                        onChange={val => this.handlePropChange('replacedById', val)}
                    />
                </FormGroup>
                <Divider />
                <FormGroup>
                    <FileUploader
                        label={templatePackage}
                        file={d.get('package')}
                        onChange={val => this.handlePropChange('package', val)}
                        attention={!this.state.packageIsValid}
                    />
                </FormGroup>
                <FormGroup>
                    <FileUploader
                        label={thumbnail}
                        file={d.get('thumb')}
                        onChange={val => this.handlePropChange('thumb', val)}
                        attention={!this.state.thumbnailIsValid}
                    />
                </FormGroup>
                <FormGroup>
                    <div style={{marginBottom: 16}}>
                        <ControlLabel>
                            {fields}
                        </ControlLabel>
                    </div>
                    {this.props.fields.map(field => {
                        const id = field.get('id');
                        return <Checkbox
                            key={id}
                            checked={d.get('fieldIds').includes(id)}
                            label={getFieldName(this.props.locale, field)}
                            onCheck={(ev, isChecked) => this.onFieldCheck(id, isChecked)}
                        />;
                    })}
                </FormGroup>
            </Form>
        );
    }

    onFieldCheck(id, isChecked) {
        const ids = this.state.data.get('fieldIds');
        const nextIds = isChecked
            ? ids.push(id)
            : ids.filter(x => x !== id);

        this.setState({
            data: this.state.data.set('fieldIds', nextIds)
        });
    }
}
