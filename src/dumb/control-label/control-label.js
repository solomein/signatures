import React, { Component, PropTypes } from 'react';
import css from './control-label.css';

const { string, bool } = PropTypes;

export default class Label extends Component {
    static propTypes = {
        children: string,
        attention: bool
    };

    static defaultProps = {
        children: ''
    };

    shouldComponentUpdate(nextProps) {
        return this.props.children !== nextProps.children
            || this.props.attention !== nextProps.attention; 
    }

    render() {
        if (!this.props.children.length) {
            return null;
        }

        return <section className={css.root}>
            {this.props.children}
            {this.props.attention &&
                <div className={css.attention}></div>
            }
        </section>;
    }
}
