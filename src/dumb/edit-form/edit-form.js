import React, { Component, PropTypes } from 'react';
import { FormActionGroup } from 'dumb/form';
import Card from 'dumb/card';
import FlatButton from 'material-ui/FlatButton';
import find from 'lodash/find';
import css from './edit-form.css';

const { object, func, bool } = PropTypes;

export default class EditForm extends Component {
    static propTypes = {
        data: object,
        onSave: func,
        onDelete: func,
        translation: object,
        canDelete: bool
    };

    constructor(props) {
        super(props);
        this.validate = this.validateConfig();
        this.state = this.calcState(props.data);
    }

    calcState(data) {
        let state = { data };
        this.validate.forEach(v => {
            this.calcValidateState(state, v, data.get(v.dataField));
        });

        return state;
    }

    calcValidateState(state, validate, value) {
        if (validate.calcState) {
            Object.assign(state, validate.calcState(value));
        }
        else {
            const validator = validate.validator || this.defaultValidator;
            state[validate.stateField] = validator(value);
        }

        return state;
    }

    validateConfig() {
        return [];
    }

    defaultValidator(data) {
        return Boolean(data);
    }

    stateIsValid() {
        return this.validate.every(v => this.state[v.stateField]);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.data) {
            this.setState(this.calcState(nextProps.data));
        }
    }

    shouldComponentUpdate(nextProps, nextState) {
        return nextProps.data !== this.props.data
            || nextState.data !== this.state.data
            || nextState.confirmDelete !== this.state.confirmDelete
            || this.validate.some(
                v => this.state[v.stateField] !== nextState[v.stateField]
            );
    }

    isNew() {
        return !this.state.data.get('id');
    }

    render() {
        return <Card>
            {this.renderMainForm(this.state.data)}
            <FormActionGroup>
                {this.renderControls()}
            </FormActionGroup>
        </Card>
    }

    renderControls() {
        const {
            createButton,
            updateButton,
            deleteButton,
            cancelButton,
            confirmDelete
        } = this.props.translation.entityEdit;

        if (this.state.confirmDelete) {
            return <div>
                <div className={css.message}>
                    {confirmDelete}
                </div>
                <FlatButton
                    onTouchTap={() => this.handleDelete()}
                    label={deleteButton}
                />
                <FlatButton
                    onTouchTap={() => this.handleCancelDelete()}
                    label={cancelButton}
                />
            </div>
        }

        if (this.isNew()) {
            return <FlatButton
                disabled={!this.stateIsValid()}
                onTouchTap={() => this.handleSave()}
                label={createButton}
            />;
        }
        else {
            return <div>
                <FlatButton
                    disabled={!this.stateIsValid()}
                    onTouchTap={() => this.handleSave()}
                    label={updateButton}
                />
                {this.props.canDelete !== false &&
                <FlatButton
                    label={deleteButton}
                    onTouchTap={() => this.handleTryDelete()}
                />
                }
            </div>;
        }
    }

    handlePropChange(propName, value) {
        const state = {
            data: this.state.data.set(propName, value)
        };

        const validate = find(this.validate, { dataField: propName });
        if (validate) {
            this.calcValidateState(state, validate, value);
        }

        this.setState(state)
    }

    handleTryDelete() {
        this.setState({
            confirmDelete: true
        });
    }

    handleCancelDelete() {
        this.setState({
            confirmDelete: false
        });
    }

    handleDelete() {
        this.props.onDelete(this.state.data.get('id'));
    }

    handleSave() {
        this.props.onSave(this.state.data);
    }

    getData() {
        return this.state.data;
    }
}
