import React, { Component, PropTypes } from 'react';
import ImageIcon from 'material-ui/svg-icons/image/image';
import ClearIcon from 'material-ui/svg-icons/action/highlight-off';
import UploadIcon from 'material-ui/svg-icons/file/file-upload';
import css from './file-uploader.css';
import { ControlLabel } from 'dumb/control-label';
import Uploader from 'utils/file-uploader';
import { file } from 'provider';
import { Map } from 'immutable';
import translate from 'i18n/translate';

const { object, func, string, bool } = PropTypes;

@translate('fileUploader')
export default class FileUploader extends Component {
    static propTypes = {
        label: string,
        file: object,
        onChange: func,
        attention: bool,
        translation: object
    };

    constructor(props) {
        super(props);
        this.uploader = new Uploader({
            request: file.upload,
            onUpload: this.handleMapUploadStart.bind(this),
            onSuccess: this.handleMapUploadSuccess.bind(this),
            onFail: this.handleMapUploadFail.bind(this),
            onAllComplete: () => {}
        });
        this.state = { file: props.file };
    }

    componentWillUnmount() {
        this.uploader.destroy();
    }

    render() {
        return <section>
            <div className={css.label}>
                <ControlLabel attention={this.props.attention}>
                    {this.props.label}
                </ControlLabel>
            </div>
            {this.state.file &&
            <div className={css.image} onTouchTap={() => this.handleFileClick()}>
                <div className={css.icon}>
                    <ImageIcon color="#e0e0e0"/>
                </div>
                <div className={css.imageName}>
                    {this.state.file.get('name')}
                </div>
                <div className={css.remove}>
                    <ClearIcon onTouchTap={ev => this.handleClear(ev)}/>
                </div>
            </div>}
            {!this.state.file &&
            <div className={css.image} onTouchTap={() => this.handleUploadClick()}>
                <div className={css.icon}>
                    <UploadIcon />
                </div>
                <div className={css.imageName}>
                    {this.props.translation.fileUploader.uploadButton}
                </div>
            </div>}
        </section>
    }

    handleUploadClick() {
        this.uploader.openDialog();
    }

    handleFileClick() {
        file.download(this.state.file.get('id'));
    }

    handleMapUploadStart() { }

    handleMapUploadSuccess(file) {
        const data = Map(file);
        this.setState({ file: data });
        this.props.onChange(data);
    }

    handleMapUploadFail() {
        //this.props.onError(error.message);
    }

    handleClear(ev) {
        ev.stopPropagation();
        this.setState({
            file: null
        });
        this.props.onChange(null);
    }
}
