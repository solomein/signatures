
import React, { Component, PropTypes } from 'react';
import AppBar from 'dumb/app-bar';

import css from './layout-with-appbar.css';


const { object, element, func } = PropTypes;

export default class LayoutWithAppbar extends Component {
    static propTypes = {

    };

    render() {
        return (
            <section>
                <AppBar>
                    {this.props.appBarElements}
                </AppBar>
                <div className={css.content}>
                    {this.props.children}
                </div>
            </section>
        );
    }
}
