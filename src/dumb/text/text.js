import React, { Component, PropTypes } from 'react';
import css from './text.css';
import { withMods } from 'utils/cssm';

const mod = withMods(css);
const { string, array, oneOfType } = PropTypes;

export default class Text extends Component {
    static propTypes = {
        type: string,
        children: oneOfType([string, array])
    };

    render() {
        return (
            <div className={mod.root({ type: this.props.type })}>
                {this.props.children}
            </div>
       );
    }
}
