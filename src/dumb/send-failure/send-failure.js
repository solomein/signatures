import React, { Component, PropTypes } from 'react';
import ErrorIcon from 'material-ui/svg-icons/navigation/close';
import css from './send-failure.css';
import translate from 'i18n/translate';
import BackIcon from 'material-ui/svg-icons/navigation/arrow-back';
import FlatButton from 'material-ui/FlatButton';

@translate('templateSetup')
export default class SendFailure extends Component {
    static propTypes = {
        translation: PropTypes.object
    };

    render() {
        return <div className={css.root}>
            <div className={css.circle}>
                <ErrorIcon
                    style={{
                        width: 50,
                        height: 50,
                        verticalAlign: 'middle'
                    }}
                    color='#F44336'
                />
            </div>
            <div className={css.title}>
                {this.props.translation.templateSetup.sendFailureTitle}
            </div>
            <div className={css.text}>
                {this.props.translation.templateSetup.sendFailureText}
            </div>
            <FlatButton
                label={this.props.translation.templateSetup.backButton}
                icon={<BackIcon />}
                onTouchTap={() => this.props.onBack()}
            />
        </div>
    }
}
