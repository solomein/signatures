import React from 'react';
import { Form, FormGroup } from 'dumb/form';
import { TextField } from 'dumb/text-field';
import EditForm from 'dumb/edit-form';
import Dropdown from 'dumb/dropdown';
import translate from 'i18n/translate';
import Divider from 'material-ui/Divider';
import { getFieldName, getTemplateName } from 'i18n/helpers';

@translate('entityEdit', 'adminSignatoryEdit')
export default class SignatoryEdit extends EditForm {
    constructor(props) {
        super(props);

        // IE HACK
        if (!this.state) {
            EditForm.call(this, props);
        }
    }

    validateConfig() {
        return [
            {
                dataField: 'email',
                stateField: 'emailIsValid'
            }
        ];
    }

    renderMainForm(d) {
        const {
            email,
            emailPlaceholder,
            template
        } = this.props.translation.adminSignatoryEdit;

        return (
            <Form>
                <FormGroup>
                    <TextField
                        label={email}
                        placeholder={emailPlaceholder}
                        autofocus={true}
                        value={d.get('email')}
                        onChange={val => this.handlePropChange('email', val)}
                        attention={!this.state.emailIsValid}
                    />
                </FormGroup>
                <Divider />
                <FormGroup>
                    <Dropdown
                        dropdownStyle={{width: '100%'}}
                        //ensureValue={true}
                        value={d.getIn(['template', 'id'])}
                        data={this.props.templates}
                        label={template}
                        dataConverter={item => {
                            return {
                                value: item.get('id'),
                                text: getTemplateName(this.props.locale, item)
                            };
                        }}
                        onChange={val => this.handleTemplateChange(val)}
                    />
                </FormGroup>
                {this.renderFields(d)}
            </Form>
        );
    }

    renderFields(d) {
        const fields = d.getIn(['template', 'fields']);
        if (this.props.hideFields || !fields || !fields.size) {
            return null;
        }

        return fields.map((field, i) => {
            return <FormGroup key={field.get('key')}>
                <TextField
                    value={this.state.data.getIn(['template', 'fields', i, 'value'])}
                    label={getFieldName(this.props.locale, field)}
                    placeholder={getFieldName(this.props.locale, field)}
                    onChange={val => this.handleFieldChange(i, val)}
                />
            </FormGroup>
        });
    }

    handleTemplateChange(templateId) {
        const currentId = this.props.data.getIn(['template', 'id']);
        if (currentId === templateId) {
            return;
        }

        this.props.onTemplateChange(templateId);
    }

    handleFieldChange(index, value) {
        this.setState({
            data: this.state.data.setIn(['template', 'fields', index, 'value'], value)
        });
    }
}
