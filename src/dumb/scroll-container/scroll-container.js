import React, { Component, PropTypes } from 'react';
import isFunction from 'lodash/isFunction';
import css from './scroll-container.css';

const { node, func, string, object } = PropTypes;

export default class ScrollContainer extends Component {
    static propTypes = {
        children: node,
        onBounce: func,
        className: string,
        style: object
    };

    componentDidMount() {
        if (isFunction(this.props.onBounce)) {
            this._unwatch = watch(this.refs.root, this.props.onBounce);
        }
    }

    componentWillUnmount() {
        if (isFunction(this._unwatch)) {
            this._unwatch();
        }
    }

    render() {
        const { className, style } = this.props;

        return <div className={className || css.root} style={style} ref="root">
            {this.props.children}
        </div>;
    }
}

function watch(el, callback) {
    let prev = 0;

    const isMoveDown = cur => cur >= prev;

    const scrollHandler = () => {
        const curPosition = el.scrollTop;
        const totalHeight = el.scrollHeight;
        const runnerBottomPosition = el.scrollTop + el.offsetHeight;

        if (isMoveDown(curPosition) && runnerBottomPosition >= totalHeight) {
            callback();
        }

        prev = curPosition;
    };

    el.addEventListener('scroll', scrollHandler, false);

    return () => el.removeEventListener('scroll', scrollHandler);
}
