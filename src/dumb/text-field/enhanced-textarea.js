import React from 'react';
import EnhancedInput from './enhanced-input';
import css from './enhanced-input.css';
import { withMods } from 'utils/cssm';

const mod = withMods(css);

export default class EnhancedTextarea extends EnhancedInput {
    componentDidMount() {
        this.correctHeight(this.refs.control);
    }

    componentDidUpdate() {
        this.correctHeight(this.refs.control);
    }

    /** @override */
    renderControl() {
        return (
            <textarea
                rows={1}
                className={mod.control({ multiline: true })}
                value={this.state.value}
                placeholder={this.props.placeholder}
                disabled={this.props.disabled}
                onChange={ev => this.handleChange(ev)}
                onFocus={this.props.onFocus}
                onBlur={this.props.onBlur}
                onKeyPress={this.props.onKeyPress}
                autoFocus={this.props.autoFocus}
                ref="control"
            />
        );
    }

    correctHeight(element) {
        const offset = element.offsetHeight - element.clientHeight;
        element.style.height = 'auto';

        const h = element.scrollHeight + offset;
        if (h) {
            element.style.height = `${h}px`;
        }
    }
}
