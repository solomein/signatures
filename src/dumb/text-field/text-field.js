import React, { Component } from 'react';
import EnhancedInput from './enhanced-input';
import EnhancedTextarea from './enhanced-textarea';

const { func, string, bool } = React.PropTypes;

export default class TextField extends Component {
    static propTypes = {
        onChange: func.isRequired,
        onKeyPress: func,
        value: string,
        multiline: bool,
        placeholder: string,
        label: string,
        disabled: bool,
        onFocus: func,
        onBlur: func,
        invalid: bool,
        message: string,
        autoFocus: bool,
        type: string,
        attention: bool
    };

    static defaultProps = {
        multiline: false
    };

    shouldComponentUpdate(nextProps) {
        return nextProps.value !== this.props.value
            || nextProps.disabled !== this.props.disabled
            || nextProps.placeholder !== this.props.placeholder
            || nextProps.attention !== this.props.attention
            || nextProps.label !== this.props.label;
    }

    render() {
        return this.props.multiline
            ? React.createElement(EnhancedTextarea, { ...this.props })
            : React.createElement(EnhancedInput, {...this.props});
    }
}
