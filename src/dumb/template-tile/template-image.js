import React, { Component, PropTypes } from 'react';
import css from './template-image.css';
import { withMods } from 'utils/cssm';

const mod = withMods(css);
const { string, bool } = PropTypes;

export default class TemplateImage extends Component {
    static propTypes = {
        imageSrc: string,
        focused: bool
    };

    render() {
        return <div className={mod.imageContainer({ focused: this.props.focused })}>
            <img src={this.props.imageSrc} className={css.image} />
        </div>;
    }
}
