import React, { Component, PropTypes } from 'react';
import css from './template-tile.css';
import TemplateImage from './template-image';
import Card from 'dumb/card';
import { Form } from 'dumb/form';
import Divider from 'material-ui/Divider';

const { string, func, bool, element } = PropTypes;

export default class TemplateTile extends Component {
    static propTypes = {
        title: string,
        image: string,
        actionType: string,
        onAction: func,
        focused: bool,
        button: element
    };

    render() {
        return <Card>
            <Form>
                <div className={css.titleWrap}>
                    <div className={css.title}>{this.props.title}</div>
                    {this.props.button}
                </div>
            </Form>
            <Divider />
            <TemplateImage imageSrc={this.props.image} focused={this.props.focused}/>
        </Card>;
    }
}
