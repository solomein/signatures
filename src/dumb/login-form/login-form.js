import React, { Component, PropTypes } from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import { TextField } from 'dumb/text-field';
import Card from 'dumb/card';
import Dropdown from 'dumb/dropdown';
import translate from 'i18n/translate';
import css from './login-form.css';

const { func, array, string , object, bool } = PropTypes;

@translate('login')
export default class LoginForm extends Component {
    static propTypes = {
        onSubmit: func.isRequired,
        onForgotClick: func.isRequired,
        languages: array,
        onChangeLocale: func,
        defaultLocale: string,
        translation: object,
        disabled: bool
    };

    constructor(props) {
        super(props);
        this.state = {
            login: '',
            password: '',
            locale: props.defaultLocale || props.languages[0].locale
        };
    }

    render() {
        const {
            email,
            password,
            language,
            enterButton,
            forgotPassword
        } = this.props.translation.login;

        return <Card>
            <div className={css.form}>
                <div className={css.logo}></div>
                <div className={css.input}>
                    <TextField
                        value={this.state.login}
                        placeholder={email}
                        onChange={val => this.handleLoginChange(val)}
                        onKeyPress={ev => this.handleKeyPress(ev)}
                        disabled={this.props.disabled}
                    />
                </div>
                <div className={css.input}>
                    <TextField
                        value={this.state.password}
                        placeholder={password}
                        type="password"
                        onChange={val => this.handlePasswordChange(val)}
                        onKeyPress={ev => this.handleKeyPress(ev)}
                        disabled={this.props.disabled}
                    />
                </div>
                <div className={css.input}>
                    <Dropdown
                        dropdownStyle={{ width: '100%' }}
                        value={this.state.locale}
                        data={this.props.languages}
                        label={language}
                        dataConverter={data => {
                            return {
                                text: data.name,
                                value: data.locale
                            };
                        }}
                        onChange={val => this.handleChangeLocale(val)}
                    />
                </div>
                <div className={css.button}>
                    <RaisedButton
                        label={enterButton}
                        primary={true}
                        onTouchTap={this.handleSubmit.bind(this)}
                        disabled={!(this.state.password && this.state.login) || this.props.disabled}
                    />
                </div>
                <div className={css.forgot}>
                    <span
                        className={css.forgotLink}
                        onTouchTap={() => this.props.onForgotClick()}
                    >
                        {forgotPassword}
                    </span>
                </div>
            </div>
        </Card>;
    }

    handleLoginChange(login) {
        this.setState({ login });
    }

    handlePasswordChange(password) {
        this.setState({ password });
    }

    handleChangeLocale(locale) {
        this.setState({ locale });
        this.props.onChangeLocale(locale);
    }

    handleKeyPress(ev) {
        if (ev.key === 'Enter') {
            this.handleSubmit();
        }
    }

    handleSubmit() {
        this.props.onSubmit(this.state.login, this.state.password, this.state.locale);
    }
}
