import React, { Component } from 'react';
import { default as ToggleMU } from 'material-ui/Toggle';

export default class Toggle extends Component {
    render() {
        const props = {
            labelStyle: { fontWeight: 300 },
            ...this.props
        };

        return <ToggleMU {...props} />;
    }
}
