import React, { PropTypes } from 'react';
import css from './bar.css';

const { node, number } = PropTypes;

export default function Bar({ children, offset }) {
    return <div style={{ top: 64 * offset }} className={css.root}>
        {children}
    </div>;
}

Bar.propTypes = {
    children: node,
    offset: number
};
