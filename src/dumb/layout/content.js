import React, { PropTypes } from 'react';
import css from './content.css';

const { number, node } = PropTypes;

export default function Content({ offset, children }) {
    return <div style={{ paddingTop: 64 * offset + 32 }} className={css.root}>
        <div className={css.content}>{children}</div>
    </div>;
}

Content.propTypes = {
    offset: number,
    children: node
};
