import React, { Component, PropTypes } from 'react';
import DoneIcon from 'material-ui/svg-icons/action/done';
import css from './send-success.css';
import translate from 'i18n/translate';
import BackIcon from 'material-ui/svg-icons/navigation/arrow-back';
import FlatButton from 'material-ui/FlatButton';

@translate('templateSetup')
export default class SendSuccess extends Component {
    static propTypes = {
        translation: PropTypes.object,
    };

    render() {
        return <div className={css.root}>
            <div className={css.circle}>
                <DoneIcon
                    style={{
                        width: 50,
                        height: 50,
                        verticalAlign: 'middle'
                    }}
                    color='#4CAF50'
                />
            </div>
            <div className={css.title}>
                {this.props.translation.templateSetup.sendSuccessTitle}
            </div>
            <div className={css.text}>
                {this.props.translation.templateSetup.sendSuccessText}
            </div>
            <FlatButton
                label={this.props.translation.templateSetup.backButton}
                icon={<BackIcon />}
                onTouchTap={() => this.props.onBack()}
            />
        </div>
    }
}
