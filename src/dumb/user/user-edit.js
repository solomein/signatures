import React from 'react';
import { Form, FormGroup } from 'dumb/form';
import { TextField } from 'dumb/text-field';
import Divider from 'material-ui/Divider';
import EditForm from 'dumb/edit-form';
import translate from 'i18n/translate';
import Toggle from 'dumb/Toggle';
import { isEmail } from 'utils/string-test';

@translate('userEdit', 'entityEdit')
export default class UserEdit extends EditForm {
    constructor(props) {
        super(props);

        // IE HACK
        if (!this.state) {
            EditForm.call(this, props);
        }
    }

    validateConfig() {
        return [
            {
                dataField: 'firstName',
                stateField: 'firstNameIsValid'
            },
            {
                dataField: 'lastName',
                stateField: 'lastNameIsValid'
            },
            {
                dataField: 'email',
                stateField: 'emailIsValid',
                validator: v => isEmail(v)
            },
            {
                dataField: 'currentPassword',
                stateField: 'currentPasswordIsValid',
                calcState: (v) => {
                    if (!this.state) {
                        return { currentPasswordIsValid: true }
                    }

                    const newpass = this.state.data.get('newPassword');
                    const confirm = this.state.data.get('confirmPassword');
                    const allEmpty = !v && !newpass && !confirm;

                    return {
                        currentPasswordIsValid: allEmpty || !!v,
                        newPasswordIsValid: allEmpty || !!newpass,
                        confirmPasswordIsValid: allEmpty || !!confirm && confirm === newpass
                    };
                }
            },
            {
                dataField: 'newPassword',
                stateField: 'newPasswordIsValid',
                calcState: (v) => {
                    if (!this.state) {
                        return { newPasswordIsValid: true };
                    }

                    const confirm = this.state.data.get('confirmPassword');
                    const current = this.state.data.get('currentPassword');
                    const allEmpty = !v && !confirm && !current;

                    return {
                        currentPasswordIsValid: allEmpty || !!current,
                        newPasswordIsValid: allEmpty || !!v,
                        confirmPasswordIsValid: allEmpty || !!confirm && v === confirm
                    };
                }
            },
            {
                dataField: 'confirmPassword',
                stateField: 'confirmPasswordIsValid',
                calcState: (v) => {
                    if (!this.state) {
                        return { confirmPasswordIsValid: true };
                    }

                    const newpass = this.state.data.get('newPassword');
                    const current = this.state.data.get('currentPassword');
                    const allEmpty = !v && !newpass && !current;

                    return {
                        currentPasswordIsValid: allEmpty || !!current,
                        newPasswordIsValid: allEmpty || !!newpass,
                        confirmPasswordIsValid: allEmpty || !!v && v === newpass
                    };
                }
            }
        ];
    }

    renderMainForm(d) {
        const {
            firstName,
            firstNamePlaceholder,
            lastName,
            lastNamePlaceholder,
            email,
            emailPlaceholder,
            currentPassword,
            newPassword,
            confirmPassword,
            passwordPlaceholder,
            isActive
        } = this.props.translation.userEdit;
        return (
            <div>
                <Form>
                    <FormGroup>
                        <TextField
                            label={firstName}
                            placeholder={firstNamePlaceholder}
                            autofocus={true}
                            value={d.get('firstName')}
                            onChange={val => this.handlePropChange('firstName', val)}
                            attention={!this.state.firstNameIsValid}
                        />
                    </FormGroup>
                    <FormGroup>
                        <TextField
                            label={lastName}
                            placeholder={lastNamePlaceholder}
                            value={d.get('lastName')}
                            onChange={val => this.handlePropChange('lastName', val)}
                            attention={!this.state.lastNameIsValid}
                        />
                    </FormGroup>
                    <FormGroup>
                        <TextField
                            label={email}
                            placeholder={emailPlaceholder}
                            value={d.get('email')}
                            onChange={val => this.handlePropChange('email', val)}
                            attention={!this.state.emailIsValid}
                        />
                    </FormGroup>
                </Form>
                <Divider />
                <Form>
                    <FormGroup>
                        <TextField
                            type="password"
                            label={currentPassword}
                            placeholder={passwordPlaceholder}
                            value={d.get('currentPassword')}
                            onChange={val => this.handlePropChange('currentPassword', val)}
                            attention={!this.state.currentPasswordIsValid}
                        />
                    </FormGroup>
                    <FormGroup>
                        <TextField
                            type="password"
                            label={newPassword}
                            placeholder={passwordPlaceholder}
                            value={d.get('newPassword')}
                            onChange={val => this.handlePropChange('newPassword', val)}
                            attention={!this.state.newPasswordIsValid}
                        />
                    </FormGroup>
                    <FormGroup>
                        <TextField
                            type="password"
                            label={confirmPassword}
                            placeholder={passwordPlaceholder}
                            value={d.get('confirmPassword')}
                            onChange={val => this.handlePropChange('confirmPassword', val)}
                            attention={!this.state.confirmPasswordIsValid}
                        />
                    </FormGroup>
                </Form>
                <Divider />
                {this.props.canEditRights && <Form>
                    <FormGroup>
                        <Toggle
                            defaultToggled={d.get('isActive')}
                            label={isActive}
                            onTouchTap={() => this.handlePropChange('isActive', !d.get('isActive'))}
                        />
                    </FormGroup>
                </Form>}
                <Divider />
            </div>
        );
    }
}
