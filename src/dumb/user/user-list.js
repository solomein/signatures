import React, { Component, PropTypes } from 'react';
import { List } from 'dumb/list';

const { object, func } = PropTypes;

export default class UserList extends Component {
    static propTypes = {
        data: object,
        onSelect: func
    };

    render() {
        return (
            <List
                data={this.props.data}
                onSelect={data => this.props.onSelect(data)}
                dataConverter={item => {
                    return {
                        id: item.get('id'),
                        primaryText: `${item.get('firstName')} ${item.get('secondName')}`,
                        secondaryText: item.get('email')
                    }
                }}
            />
        );
    }
}
