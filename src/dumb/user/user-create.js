import React from 'react';
import { Form, FormGroup } from 'dumb/form';
import { TextField } from 'dumb/text-field';
import Divider from 'material-ui/Divider';
import EditForm from 'dumb/edit-form';
import translate from 'i18n/translate';
import Toggle from 'dumb/Toggle';
import { isEmail } from 'utils/string-test';

@translate('userEdit', 'entityEdit')
export default class UserCreate extends EditForm {
    constructor(props) {
        super(props);

        // IE HACK
        if (!this.state) {
            EditForm.call(this, props);
        }
    }

    validateConfig() {
        return [
            {
                dataField: 'firstName',
                stateField: 'firstNameIsValid'
            },
            {
                dataField: 'lastName',
                stateField: 'lastNameIsValid'
            },
            {
                dataField: 'email',
                stateField: 'emailIsValid',
                validator: v => isEmail(v)
            },
            {
                dataField: 'password',
                stateField: 'passwordIsValid',
                calcState: (v) => {
                    if (!this.state) {
                        return { passwordIsValid: false };
                    }

                    const confirm = this.state.data.get('confirmPassword');
                    return {
                        passwordIsValid: !!v,
                        confirmPasswordIsValid: confirm && (v === confirm || !v)
                    };
                }
            },
            {
                dataField: 'confirmPassword',
                stateField: 'confirmPasswordIsValid',
                calcState: (v) => {
                    if (!this.state) {
                        return { confirmPasswordIsValid: false };
                    }

                    const password = this.state.data.get('password');
                    return { confirmPasswordIsValid: v && (v === password || !password) };
                }
            }
        ];
    }

    passwordValidator(pass, confirm) {
        if (!this.state) {
            return true;
        }

        const { data } = this.state;

        const password = data.get('password');

        if (!password) {
            return true;
        }

        return (pass || password) === (confirm || data.get('confirmPassword'));
    }

    renderMainForm(d) {
        const {
            firstName,
            firstNamePlaceholder,
            lastName,
            lastNamePlaceholder,
            email,
            emailPlaceholder,
            confirmPassword,
            password,
            passwordPlaceholder,
            isActive
        } = this.props.translation.userEdit;

        return (
            <div>
                <Form>
                    <FormGroup>
                        <TextField
                            label={firstName}
                            placeholder={firstNamePlaceholder}
                            autofocus={true}
                            value={d.get('firstName')}
                            onChange={val => this.handlePropChange('firstName', val)}
                            attention={!this.state.firstNameIsValid}
                        />
                    </FormGroup>
                    <FormGroup>
                        <TextField
                            label={lastName}
                            placeholder={lastNamePlaceholder}
                            value={d.get('lastName')}
                            onChange={val => this.handlePropChange('lastName', val)}
                            attention={!this.state.lastNameIsValid}
                        />
                    </FormGroup>
                    <FormGroup>
                        <TextField
                            label={email}
                            placeholder={emailPlaceholder}
                            value={d.get('email')}
                            onChange={val => this.handlePropChange('email', val)}
                            attention={!this.state.emailIsValid}
                        />
                    </FormGroup>
                </Form>
                <Divider />
                <Form>
                    <FormGroup>
                        <TextField
                            type="password"
                            label={password}
                            placeholder={passwordPlaceholder}
                            value={d.get('password')}
                            onChange={val => this.handlePropChange('password', val)}
                            attention={!this.state.passwordIsValid}
                        />
                    </FormGroup>
                    <FormGroup>
                        <TextField
                            type="password"
                            label={confirmPassword}
                            placeholder={passwordPlaceholder}
                            value={d.get('confirmPassword')}
                            onChange={val => this.handlePropChange('confirmPassword', val)}
                            attention={!this.state.confirmPasswordIsValid}
                        />
                    </FormGroup>
                </Form>
                <Divider />
                <Form>
                    <FormGroup>
                        <Toggle
                            defaultToggled={d.get('isActive')}
                            label={isActive}
                            onTouchTap={() => this.handlePropChange('isActive', !d.get('isActive'))}
                        />
                    </FormGroup>
                </Form>
                <Divider />
            </div>
        );
    }
}
