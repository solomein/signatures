import React, { Component, PropTypes } from 'react';
import Text from 'dumb/text';
import css from './table.css';
import { withMods } from 'utils/cssm';

const mod = withMods(css);

export class Table extends Component {
    render() {
        const className = mod.table({
            selectable: this.props.selectable,
            divided: this.props.divided
        });

        return <div className={css.tableWrap}>
            <table className={className}>{this.props.children}</table>
        </div>;
    }
}

export class TableRow extends Component {
    render() {
        return <tr className={css.row} onTouchTap={this.props.onTouchTap}>{this.props.children}</tr>;
    }
}

export class TableCell extends Component {
    render() {
        const width = this.props.width || 'auto';
        return <td className={css.cell} style={{ width }}>
            {this.props.children}
        </td>;
    }
}

export class TableHeaderRow extends Component {
    render() {
        return <tr className={css.headerRow}>{this.props.children}</tr>;
    }
}

export class TableHeaderCell extends Component {
    render() {
        return <th className={css.headerCell}>
            <Text type="caption">{this.props.children}</Text>
        </th>;
    }
}
