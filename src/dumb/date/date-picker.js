import React, { Component, PropTypes } from 'react';
import { default as Picker }from 'material-ui/DatePicker';
import { ControlLabel } from 'dumb/control-label';
import translate from 'i18n/translate';
import dateFormatter from 'i18n/date-formatter';

const { string, object, number, oneOfType, func } = PropTypes;

@translate()
export default class DatePicker extends Component {
    static propTypes = {
        locale: string,
        label: string,
        value: oneOfType([object, number, string]),
        onChange: func
    };

    render() {
        return <div>
            <ControlLabel>{this.props.label}</ControlLabel>
            <Picker
                locale={this.props.locale}
                hintText="Date"
                container="inline"
                DateTimeFormat={Intl.DateTimeFormat}
                defaultDate={this.getValue()}
                onChange={(ev, date) => this.props.onChange(date)}
                formatDate={date => {
                    return dateFormatter(this.props.locale, date);
                }}
                style={{marginTop: -8}}
            />
        </div>;
    }

    getValue() {
        return this.props.value ? new Date(this.props.value) : new Date();
    }
}
