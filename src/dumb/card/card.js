import React, { Component, PropTypes } from 'react';
//import Paper from 'material-ui/Paper';
import css from './card.css';

export default class Card extends Component {
    render() {
        return (
            <section className={css.root}>
                <div className={css.paper}>
                    {this.props.children}
                </div>
            </section>
        );
    }
}
