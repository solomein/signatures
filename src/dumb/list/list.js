import React, { Component, PropTypes } from 'react';
import { default as MaterialList } from 'material-ui/List';
import ListItem from './list-item';

const { func, object } = PropTypes;

export default class List extends Component {
    static propTypes = {
        onSelect: func.isRequired,
        data: object.isRequired,
        dataConverter: func,
        handleData: func
    };

    render() {
        return <MaterialList>
            {this.renderEntities()}
        </MaterialList>;
    }

    renderEntities() {
        return this.props.data.map((entity, i) => {
            const d = (this.props.dataConverter || this.dataConverter)(entity);
            return <ListItem
                key={d.id}
                id={d.id}
                index={i}
                primaryText={d.primaryText}
                secondaryText={d.secondaryText}
                onSelect={data => this.handleSelect(data, i)}
                avatar={d.avatar}
                controls={d.controls}
                entity={entity}
                handleData={this.props.handleData}
            />;
        });
    }

    dataConverter(entity) {
        return {
            id: entity.get('id'),
            primaryText: entity.get('primaryText'),
            secondaryText: entity.get('secondaryText')
        };
    }

    handleSelect(data, i) {
        this.props.onSelect(data, i);
    }
}
