import React, { Component, PropTypes } from 'react';
//import Avatar from 'material-ui/Avatar';
// import IconButton from 'material-ui/IconButton';
// import DeleteIcon from 'material-ui/svg-icons/action/delete';
// import DragHandleIcon from 'material-ui/svg-icons/editor/drag-handle';
import css from './list-item.css';
import { withMods } from 'utils/cssm';


const mod = withMods(css);
const { func, string, element, node, object } = PropTypes;

export default class EntityItem extends Component {
    static propTypes = {
        onSelect: func.isRequired,
        id: string.isRequired,
        primaryText: node.isRequired,
        secondaryText: node,
        avatar: element,
        controls: node,
        handleData: func,
        entity: object
    };

    render() {
        return (
            <div
                className={mod.root()}
            >
                {this.renderAvatar()}
                <div className={css.text} onTouchTap={() => {this.handleSelect()}}>
                    <div className={css.primary}>
                        {this.props.primaryText}
                    </div>
                    {this.props.secondaryText &&
                        <div className={css.secondary}>
                            {this.props.secondaryText}
                        </div>
                    }
                </div>
                {this.renderControls()}
                <div className={css.bg}></div>
            </div>
       );
    }

    renderAvatar() {
        if (!this.props.avatar) {
            return;
        }

        return <div className={css.avatar}>{this.props.avatar}</div>;
    }

    renderControls() {
        const { controls } = this.props;

        if (!controls) {
            return;
        }

        return <div className={css.controls}>
            {controls.map((control, i) => <div key={i}>{control}</div>)}
        </div>;
    }

    handleSelect() {
        const { onSelect, handleData, entity, id } = this.props;
        onSelect(handleData ? handleData(entity) : { id });
    }
}
