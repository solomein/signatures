import React, { Component, PropTypes } from 'react';
import css from './app-bar-cell.css';
import { withMods } from 'utils/cssm';

const mod = withMods(css);
// const { string, func, bool } = PropTypes;

export default class AppBarCell extends Component {
    static propTypes = {};

    static defaultProps = {
        width: 'auto'
    };

    render() {
        return <div className={css.root} style={{ width: this.props.width }}>
            {this.props.children}
        </div>;
    }
}
