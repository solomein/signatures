import React, { Component, PropTypes } from 'react';
import css from './app-bar.css';
import { withMods } from 'utils/cssm';

const mod = withMods(css);
// const { string, func, bool } = PropTypes;

export default class Bar extends Component {
    static propTypes = {};

    render() {
        return <section className={mod.root({ second: this.props.second})}>
            {this.props.children}
        </section>;
    }
}
