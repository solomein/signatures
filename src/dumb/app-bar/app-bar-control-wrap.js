import React, { Component, PropTypes } from 'react';
import css from './app-bar-control-wrap.css';

export default class AppBarControlWrap extends Component {
    static propTypes = {};

    render() {
        return <div className={css.root} style={this.props.style}>
            {this.props.children}
        </div>;
    }
}
