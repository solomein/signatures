import React, { Component, PropTypes } from 'react';
import css from './title.css';
import { withMods } from 'utils/cssm';

const { string, bool } = PropTypes;
const mod = withMods(css);

export default class Title extends Component {
    static propTypes = {
        children: string,
        collapsed: bool
    };

    shouldComponentUpdate(nextProps) {
        return this.props.children !== nextProps.children
            || this.props.collapsed !== nextProps.collapsed;
    }

    render() {
        return <section className={mod.root({ collapsed: this.props.collapsed })}>
            {this.props.children}
        </section>;
    }
}
