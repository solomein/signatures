import React, { Component, PropTypes } from 'react';
import css from './tab.css';
import { withMods } from 'utils/cssm';

const { string, bool, func } = PropTypes;
const mod = withMods(css);

export default class Tab extends Component {
    static propTypes = {
        children: string,
        active: bool,
        onClick: func
    };

    render() {
        return <section
            className={mod.root({ active: this.props.active })}
            onClick={() => this.props.onClick()}
        >
            {this.props.children}
        </section>;
    }
}
