import React, { Component, PropTypes } from 'react';
import css from './user.css';
import Avatar from 'material-ui/Avatar';
import Person from 'material-ui/svg-icons/social/person';
// import Popover from 'material-ui/Popover';
// import FlatButton from 'material-ui/FlatButton';
// import Settings from 'material-ui/svg-icons/action/settings';
// import ExitToApp from 'material-ui/svg-icons/action/exit-to-app';
// import Text from 'dumb/text';
import translate from 'i18n/translate';

const { string, func, object } = PropTypes;

@translate('userInAppBar')
export default class User extends Component {
    static propTypes = {
        firstName: string,
        secondName: string,
        //onSettingsTap: func,
        //onExitTap: func,
        //email: string,
        translation: object,
        onTouchTap: func
    };

    constructor(props) {
        super(props);
        this.state = {
            open: false
        };
    }

    shouldComponentUpdate(nextProps, nextState) {
        const { firstName, secondName } = this.props;

        return nextState.open !== this.state.open
            || nextProps.firstName !== firstName
            || nextProps.secondName !== secondName;
    }

    render() {
        return <div
            className={css.root}
            onTouchTap={ev => this.handleTouchTap(ev)}
        >
            <div className={css.name}>
                {this.props.email}
            </div>
            <Avatar
                backgroundColor="rgba(255, 255, 255, 0.3)"
                icon={<Person />}
                style={{ verticalAlign: 'middle' }}
            />
            {/*<Popover
                open={this.state.open}
                anchorEl={this.state.anchorEl}
                anchorOrigin={{ horizontal: 'right', vertical: 'bottom' }}
                targetOrigin={{ horizontal: 'right', vertical: 'top' }}
                onRequestClose={() => this.handleRequestClose()}
            >
                <div className={css.popover}>
                    <div className={css.popoverUser}>
                        {this.props.firstName} {this.props.secondName}
                        <Text type="body-2">{this.props.email}</Text>
                    </div>
                    <div className={css.buttonsWrap}>
                        <FlatButton
                            onTouchTap={() => this.handleSettingsTap()}
                            label={this.props.translation.userInAppBar.settingsButton}
                            icon={<Settings />}
                        />
                        <FlatButton
                            onTouchTap={() => this.handleExitTap()}
                            label={this.props.translation.userInAppBar.exitButton}
                            icon={<ExitToApp />}
                        />
                    </div>
                </div>
            </Popover>*/}
        </div>
    }

    // handleSettingsTap() {
    //     this.setState({ open: false });
    //     this.props.onSettingsTap();
    // }
    //
    // handleExitTap() {
    //     this.props.onExitTap();
    // }

    handleTouchTap() {
        this.props.onTouchTap();
        // ev.preventDefault();
        //
        // this.setState({
        //     open: true,
        //     anchorEl: ev.currentTarget
        // });
    }

    // handleRequestClose() {
    //     this.setState({ open: false });
    // }
}
