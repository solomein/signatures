import React, { Component, PropTypes } from 'react';
import IconButton from 'material-ui/IconButton';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import ChevronIcon from 'material-ui/svg-icons/navigation/chevron-right';
import MoreIcon from 'material-ui/svg-icons/navigation/more-horiz';
import css from './breadcrumbs.css';

const { object, func } = PropTypes;

export default class Breadcrumbs extends Component {
    static propTypes = {
        collapsedItems: object,
        items: object,
        onChange: func.isRequired
    };

    render() {
        return <section className={css.root}>
            {this.renderCollapsedItems(this.props.collapsedItems)}
            {this.renderItems(this.props.items)}
        </section>;
    }

    renderItems(items) {
        const crumbs = items.pop();
        const tail = items.last();

        return <div className={css.crumbs}>
            {
                crumbs.map((crumb, i) => <div className={css.crumb} key={i}>
                    <div
                        onClick={() => this.handleCrumbClick(crumb.get('url'))}
                        className={css.crumbLink}
                    >
                        {crumb.get('title')}
                    </div>
                    <ChevronIcon color="#fff" style={{ verticalAlign: 'middle' }}/>
                </div>)
            }
            <div className={css.tail}>{tail.get('title')}</div>
        </div>
    }

    renderCollapsedItems(items) {
        if (!items || !items.size) {
            return;
        }

        const button = <IconButton><MoreIcon color="#fff"/></IconButton>;

        return <div className={css.more}>
            <IconMenu
                iconButtonElement={button}
                anchorOrigin={{ horizontal: 'left', vertical: 'top' }}
                targetOrigin={{ horizontal: 'left', vertical: 'top' }}
                onChange={(ev, url) => this.handleCrumbClick(url)}
            >
                {items.map((item, i) => {
                    return <MenuItem
                        key={i}
                        primaryText={item.get('title')}
                        value={item.get('url')}
                    />;
                })}
            </IconMenu>
        </div>;
    }

    handleCrumbClick(url) {
        this.props.onChange(url);
    }
}
