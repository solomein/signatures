import React, { Component, PropTypes } from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import { TextField } from 'dumb/text-field';
import Card from 'dumb/card';
import css from './reset-password.css';
import translate from 'i18n/translate';

const { func, object } = PropTypes;

@translate('resetPassword')
export default class ResetPassword extends Component {
    static propTypes = {
        onSubmit: func.isRequired,
        onCancel: func.isRequired,
        translation: object
    };

    constructor(props) {
        super(props);
        this.state = {
            email: ''
        };
    }

    render() {
        const {
            text,
            email,
            sendButton,
            cancelButton
        } = this.props.translation.resetPassword;

        return <Card>
            <div className={css.form}>
                <div className={css.text}>
                    {text}
                </div>
                <div className={css.input}>
                    <TextField
                        value={this.state.email}
                        placeholder={email}
                        onChange={val => this.handleEmailChange(val)}
                        onKeyPress={ev => this.handleKeyPress(ev)}
                    />
                </div>
                <div className={css.buttons}>
                    <RaisedButton
                        label={sendButton}
                        primary={true}
                        onTouchTap={this.handleSubmit.bind(this)}
                        disabled={!this.state.email}
                    />
                    <FlatButton
                        label={cancelButton}
                        onTouchTap={this.props.onCancel}
                        style={{ marginLeft: 8, height: 36 }}
                    />
                </div>
            </div>
        </Card>;
    }

    handleEmailChange(email) {
        this.setState({ email });
    }

    handleKeyPress(ev) {
        if (ev.key === 'Enter') {
            this.handleSubmit();
        }
    }

    handleSubmit() {
        this.props.onSubmit(this.state.email);
    }
}
