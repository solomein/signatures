import React, { Component, PropTypes } from 'react';
import css from './centring.css';

const { node, object } = PropTypes;

export default class Centring extends Component {
    static propTypes = {
        children: node,
        style: object
    };

    render() {
        return <div className={css.root} style={this.props.style}>
            <div className={css.content}>
                {this.props.children}
            </div>
        </div>;
    }
}
