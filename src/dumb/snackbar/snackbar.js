import React, { Component, PropTypes } from 'react';
import Snackbar from 'material-ui/snackbar';

const { string, bool, func } = PropTypes;

export default class Snack extends Component {
    static propTypes = {
        onClose: func,
        message: string,
        open: bool
    };

    static defaultProps = {
        message: ''
    };

    constructor(props) {
        super(props);
        this.state = { open: props.open };
    }

    componentWillReceiveProps(props) {
        this.setState({ open: props.open });
    }

    shouldComponentUpdate(nextProps, nextState) {
        return nextProps.message !== this.props.message
            || nextState.open !== this.state.open;
    }

    render() {
        return <Snackbar
            open={this.state.open}
            message={this.props.message}
            autoHideDuration={2000}
            onRequestClose={() => this.handleRequestClose()}
        />;
    }

    handleRequestClose() {
        this.setState({ open: false });
        if (this.props.onClose) {
            this.props.onClose();
        }
    }
}
