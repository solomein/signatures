import { combineReducers } from 'redux-immutablejs';
import mainLayout from './main-layout-reducer';
import user from './user-reducer';
import admin from './admin-reducer';
import baseList from './base-list-reducer';
import baseEntity from './base-entity-reducer';
import template from './template-reducer';
import dynamicFieldList from './dynamic-field-list-reducer';
import signatory from './signatory-reducer';

const rootReducer = combineReducers({
    mainLayout,
    baseList,
    baseEntity,
    template,
    dynamicFieldList,
    signatory,
    admin,
    user
});

export default rootReducer;
