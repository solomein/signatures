import * as Types from 'actions/action-types';
import { createReducer } from 'redux-immutablejs';
import { fromJS } from 'immutable';
import { getLastLocale } from 'admin-auth';

const initialState = fromJS({
    interfaceLocale: getLastLocale(),
    lastName: null,
    firstName: null,
    email: null
});

export default createReducer(initialState, {
    [Types.GET_ADMIN_PROFILE_SUCCESS]: (state, { payload }) =>
        state.mergeDeep(payload),

    [Types.SET_ADMIN_LOCALE]: (state, { payload }) =>
        state.set('interfaceLocale', payload)
});
