import * as Types from 'actions/action-types';
import { createReducer } from 'redux-immutablejs';
import { fromJS } from 'immutable';

const initialState = fromJS({
    isCompleted: false,
    items: []
});

export default createReducer(initialState, {
    [Types.GET_BASE_LIST_SUCCESS]: (state, { payload }) => {
        return state.merge({
            fetching: false,
            items: payload.data,
            isCompleted: payload.expectedCount > payload.data.length
        });
    },

    [Types.GET_BASE_LIST_PART_SUCCESS]: (state, { payload }) => {
        const items = state.get('items').concat(fromJS(payload.data));
        return state.merge({
            items: items,
            isCompleted: payload.expectedCount > items.size
        });
    },

    [Types.RESET_BASE_LIST]: (state) => state.merge({
        items: [],
        isCompleted: false
    })
});
