import * as Types from 'actions/action-types';
import { createReducer } from 'redux-immutablejs';
import { fromJS } from 'immutable';
import { getLastLocale } from 'user-connect';

const initialState = fromJS({
    interfaceLocale: getLastLocale(),
    email: null,
    templateId: null,
    template: null
});

export default createReducer(initialState, {
    [Types.GET_USER_CURRENT_TEMPLATE_SUCCESS]: (state, { payload }) => {
        return state.merge({
            template: payload
        });
    },

    [Types.GET_USER_PROFILE_SUCCESS]: (state, { payload }) =>
        state.mergeDeep(payload),

    [Types.SET_USER_LOCALE]: (state, { payload }) =>
        state.set('interfaceLocale', payload)
});
