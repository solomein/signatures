export default function getHandlers({ getTypes, sendTypes, reset }) {
    return {
        [getTypes[0]]: (state) => state.merge({
            operationSucceeded: false,
            lastOperation: 'get',
            fetching: true
        }),

        [getTypes[1]]: (state, { payload }) => state.merge({
            data: payload.entity,
            associated: payload.associated,
            operationSucceeded: true,
            fetching: false
        }),

        [reset]: (state) => state.merge({
            data: null,
            associated: null,
            lastOperation: null,
            operationSucceeded: false,
            fetching: false,
            sending: false
        }),

        [sendTypes[0]]: (state, { payload }) => state.merge({
            sending: true,
            lastOperation: payload,
            operationSucceeded: false
        }),

        [sendTypes[1]]: (state) => state.merge({
            sending: false,
            operationSucceeded: true
        })
    };
}
