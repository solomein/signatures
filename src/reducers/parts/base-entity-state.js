export default {
    data: null,
    associated: null,
    sending: false,
    fetching: false,
    lastOperation: null,
    operationSucceeded: false
}
