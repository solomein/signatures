import * as Types from 'actions/action-types';
import { createReducer } from 'redux-immutablejs';
import { fromJS, Map } from 'immutable';
import uniqueId from 'lodash/uniqueId';

const initialState = fromJS({
    items: []
});

export default createReducer(initialState, {
    [Types.GET_DYNAMIC_FIELD_LIST_SUCCESS]: (state, { payload }) => {
        return state.merge({
            items: payload.data
        });
    },

    [Types.RESET_DYNAMIC_FIELD_LIST]: (state) => state.merge({
        items: []
    }),

    [Types.ADD_DYNAMIC_FIELD]: (state) => state.merge({
        items: state.get('items').insert(0, Map({
            _cid: uniqueId()
        }))
    }),

    [Types.PERSIST_DYNAMIC_FIELD]: (state, { payload }) => {
        return state.mergeIn(['items', payload.index], payload.data);
    },

    [Types.SAVE_DYNAMIC_FIELD_REQUEST]: (state, { payload }) =>
        state.setIn(['items', payload.index, 'sending'], true),

    [Types.SAVE_DYNAMIC_FIELD_SUCCESS]: (state, { payload }) => {
        return state.update('items', items => items.map(item => {
            const data = payload.data;
            const eqId = item.get('id') && item.get('id') === data.id;
            const eqCid = item.get('_cid') && item.get('_cid') === payload.cid;

            if (eqId || eqCid) {
                return item.merge({
                    ...data,
                    sending: false
                });
            }

            return item;
        }));
    },

    [Types.DELETE_DYNAMIC_FIELD_REQUEST]: (state, { payload }) =>
        state.setIn(['items', payload.index, 'sending'], true),

    [Types.DELETE_DYNAMIC_FIELD_SUCCESS]: (state, { payload }) => {
        if (payload.id) {
            return state.update(
                'items',
                items => items.filter(x => x.get('id') !== payload.id)
            );
        }

        return state.update('items', items => items.delete(payload.index));
    }
});
