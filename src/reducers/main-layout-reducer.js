import * as Types from 'actions/action-types';
import { createReducer } from 'redux-immutablejs';
import { fromJS } from 'immutable';

const initialState = fromJS({
    breadcrumbs: [],
    messages: []
});

export default createReducer(initialState, {
    [Types.SET_BREADCRUMBS]: (state, { payload }) => state.merge({
        breadcrumbs: payload
    }),

    [Types.SHOW_MESSAGE]: (state, { message }) => state.merge({
        messages: state.get('messages').push(message)
    }),

    [Types.REMOVE_MESSAGE]: (state, { stamp }) => state.merge({
        messages: state.get('messages').filter(m => m.stamp !== stamp)
    })
});
