import * as Types from 'actions/action-types';
import { createReducer } from 'redux-immutablejs';
import { fromJS } from 'immutable';

const initialState = fromJS({
    data: null,
    associated: null,
    sending: false,
    sended: false,
    error: false
});

export default createReducer(initialState, {
    [Types.GET_TEMPLATE_SUCCESS]: (state, { payload }) => state.merge({
        data: payload.entity,
        associated: payload.associated
    }),

    [Types.SETUP_TEMPLATE_REQUEST]: state => state.merge({
        sending: true
    }),

    [Types.SETUP_TEMPLATE_SUCCESS]: state => state.merge({
        sended: true,
        sending: false,
        error: false
    }),

    [Types.SETUP_TEMPLATE_FAILURE]: state => state.merge({
        sended: true,
        sending: false,
        error: true
    }),

    [Types.RESET_TEMPLATE]: (state) => state.merge({
        data: null,
        associated: null,
        sended: false,
        sending: false,
        error: false
    })
});
