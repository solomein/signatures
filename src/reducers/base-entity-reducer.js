import * as Types from 'actions/action-types';
import { createReducer } from 'redux-immutablejs';
import { fromJS } from 'immutable';
import init from './parts/base-entity-state';
import handlers from './parts/base-entity-handlers'

const initialState = fromJS(init);
export default createReducer(initialState, handlers({
    getTypes: [Types.GET_BASE_ENTITY_REQUEST, Types.GET_BASE_ENTITY_SUCCESS],
    sendTypes: [Types.SEND_BASE_ENTITY_REQUEST, Types.SEND_BASE_ENTITY_SUCCESS],
    reset: Types.RESET_BASE_ENTITY
}));
