import * as Types from 'actions/action-types';
import { createReducer } from 'redux-immutablejs';
import { fromJS } from 'immutable';
import init from './parts/base-entity-state';
import handlers from './parts/base-entity-handlers'

const initialState = fromJS({
    ...init,
    templateFetching: false
});
const baseHandlers = handlers({
    getTypes: [Types.GET_SIGNATORY_REQUEST, Types.GET_SIGNATORY_SUCCESS],
    sendTypes: [Types.SEND_SIGNATORY_REQUEST, Types.SEND_SIGNATORY_SUCCESS],
    reset: Types.RESET_SIGNATORY
})
export default createReducer(initialState, {
    ...baseHandlers,
    [Types.GET_SIGNATORY_TEMPLATE_REQUEST]: (state) =>
        state.set('templateFetching', true),

    [Types.GET_SIGNATORY_TEMPLATE_SUCCESS]: (state, { payload }) =>
        state
            .set('templateFetching', false)
            .mergeIn(['data'], { template: payload.entity })
});
