import { Component, PropTypes, createElement } from 'react';
import { i18n } from 'provider';
const { string, object } = PropTypes;

export default function translate(...keys) {
    return WrappedComponent => {
        return class TranslationComponent extends Component {
            static contextTypes = {
                userLocale: string,
                adminLocale: string,
                area: string
            };

            static propTypes = {
                translation: object
            };

            render() {
                const locale = this.context.area === 'user'
                    ? this.context.userLocale
                    : this.context.adminLocale;

                const translation = keys.length && i18n.getTranslation(locale, keys);

                return createElement(WrappedComponent, {
                    ...this.props,
                    translation,
                    locale
                });
            }
        }
    };
}
