export function getTemplateName(locale, data) {
    if (!data) {
        return '';
    }

    if (locale === 'en-GB') {
        return data.get('englishName');
    }

    if (locale === 'fr') {
        return data.get('frenchName');
    }
}

export function getFieldName(locale, data) {
    if (!data) {
        return '';
    }

    if (locale === 'en-GB') {
        return data.get('englishName');
    }

    if (locale === 'fr') {
        return data.get('frenchName');
    }
}
