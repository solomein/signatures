import { PropTypes, Component } from 'react';
import { connect } from 'react-redux';

const { string, node } = PropTypes;

const stateToProps = state => ({
    userLocale: state.getIn(['user', 'interfaceLocale']),
    adminLocale: state.getIn(['admin', 'interfaceLocale'])
});

@connect(stateToProps)
export default class TranslationProvider extends Component {
    static propTypes = {
        userLocale: string,
        adminLocale: string,
        children: node
    };

    static childContextTypes = {
        userLocale: string.isRequired,
        adminLocale: string.isRequired
    };

    render() {
        return this.props.children;
    }

    getChildContext() {
        return {
            userLocale: this.props.userLocale,
            adminLocale: this.props.adminLocale
        };
    }
}
