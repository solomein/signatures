import areIntlLocalesSupported from 'intl-locales-supported';

if (!areIntlLocalesSupported(['en-GB', 'fr'])) {
    require('intl');
    require('intl/locale-data/jsonp/en-GB');
    require('intl/locale-data/jsonp/fr');
}
