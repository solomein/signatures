import './intl';
import isString from 'lodash/isString';

export default function (locale, date, format = 'date') {
    const formats = {
        time: {
            hour: 'numeric',
            minute: '2-digit'
        },
        date: {
            year: 'numeric',
            month: '2-digit',
            day: '2-digit'
        },
        dateTime: {
            year: 'numeric',
            month: '2-digit',
            day: '2-digit',
            hour: 'numeric',
            minute: '2-digit'
        }
    };

    if (isString(date)) {
        date = new Date(date);
    }

    return new Intl.DateTimeFormat(locale, formats[format]).format(date);
}
