// export default {
//     cors: true,
//     appFakes: false,
//     appApi: '//scrat2.fwa.eu/Signature/api'
// }

export default {
    cors: false,
    appFakes: process.env.APP_FAKES,
    appApi: process.env.APP_API
}
