import {
    grey100, grey300, grey400, grey500,
    white, darkBlack, fullBlack
} from 'material-ui/styles/colors';
import { fade } from 'material-ui/utils/colorManipulator';



const accent = '#003859';
const primary = '#FFB300';

const theme = {
    spacing: {
        iconSize: 24,
        desktopGutter: 24,
        desktopGutterMore: 32,
        desktopGutterLess: 16,
        desktopGutterMini: 8,
        desktopKeylineIncrement: 64,
        desktopDropDownMenuItemHeight: 32,
        desktopDropDownMenuFontSize: 15,
        desktopDrawerMenuItemHeight: 48,
        desktopSubheaderHeight: 48,
        desktopToolbarHeight: 56
    },
    fontFamily: 'Roboto, sans-serif',
    palette: {
        primary1Color: primary,//blueGrey700,
        primary2Color: primary,
        primary3Color: grey400,
        accent1Color: accent,
        accent2Color: grey100,
        accent3Color: grey500,
        textColor: darkBlack,
        alternateTextColor: white,
        canvasColor: white,
        borderColor: grey300,
        disabledColor: fade(darkBlack, 0.3),
        pickerHeaderColor: primary,
        clockCircleColor: fade(darkBlack, 0.07),
        shadowColor: fullBlack
    }
};

export default theme;
