import fakeTemplates from './fake-templates';

export default {
    email: 'richard.roe@chryso.com',
    templateId: fakeTemplates[0].id
}
