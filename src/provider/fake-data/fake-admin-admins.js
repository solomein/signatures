import uniqueId from 'lodash/uniqueId';

const data = [
    {
        id: uniqueId(),
        firstName: 'Richard',
        lastName: 'Roe',
        isActive: true,
        email: 'richard.roe@chryso.com'
    },
    {
        id: uniqueId(),
        firstName: 'John',
        lastName: 'Doe',
        isActive: true,
        email: 'john.doe@chryso.com'
    }
];

export default data;
