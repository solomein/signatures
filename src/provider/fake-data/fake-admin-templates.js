import uniqueId from 'lodash/uniqueId';
import fakeFields from './fake-admin-dynamic-fields';
import templateLanguage from 'provider/template-language';

const lang = templateLanguage.getLanguages();

const data = [
    {
        id: uniqueId(),
        frenchName: 'Standard FWA in French',
        englishName: 'Standard FWA in French',
        lang: lang[1],
        isActive: true,
        package: {
            id: uniqueId(),
            name: 'package.zip'
        },
        thumb: {
            id: uniqueId(),
            name: 'template.png'
        },
        fieldIds: [fakeFields[0].id],
        replacedById: ''
    },
    {
        id: uniqueId(),
        frenchName: 'Chryso french signature',
        englishName: 'Chryso french signature',
        lang: lang[1],
        isActive: true,
        package: {
            id: uniqueId(),
            name: 'package.zip'
        },
        thumb: {
            id: uniqueId(),
            name: 'template.png'
        },
        fieldIds: [fakeFields[0].id]
    },
    {
        id: uniqueId(),
        frenchName: 'Versilio',
        englishName: 'Versilio',
        lang: lang[0],
        isActive: true,
        package: {
            id: uniqueId(),
            name: 'package.zip'
        },
        thumb: {
            id: uniqueId(),
            name: 'template.png'
        },
        fieldIds: [fakeFields[0].id]
    }
];

for (let i = 0; i < 20; i++) {
    data.push({
        id: uniqueId(),
        frenchName: 'Standard FWA in French',
        englishName: 'Standard FWA in French',
        lang: lang[1],
        isActive: false,
        package: {
            id: uniqueId(),
            name: 'package.zip'
        },
        thumb: {
            id: uniqueId(),
            name: 'template.png'
        },
        fieldIds: [fakeFields[0].id]
    });
}

export default data;
