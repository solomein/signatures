import uniqueId from 'lodash/uniqueId';
import fakeFields from './fake-admin-dynamic-fields';
import pic from './1.png';
import pic2 from './2.jpg';
import pic3 from './3.jpg';
import templateLanguage from 'provider/template-language';

const lang = templateLanguage.getLanguages();

const data = [
    {
        id: uniqueId(),
        frenchName: 'Standard FWA in French',
        englishName: 'Standard FWA in French',
        thumb: pic,
        fields: fakeFields,
        lang: lang[1]
    },
    {
        id: uniqueId(),
        frenchName: 'Chryso french signature',
        englishName: 'Chryso french signature',
        thumb: pic2,
        fields: [fakeFields[0]],
        lang: lang[1]
    },
    {
        id: uniqueId(),
        frenchName: 'Versilio',
        englishName: 'Versilio',
        thumb: pic3,
        fields: fakeFields,
        lang: lang[1]
    }
];

for (let i = 0; i < 20; i++) {
    data.push({
        id: uniqueId(),
        frenchName: 'Standard FWA in French',
        englishName: 'Standard FWA in French',
        thumb: pic3,
        fields: fakeFields,
        lang: lang[1]
    });
}

export default data;
