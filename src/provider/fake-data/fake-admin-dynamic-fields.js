import uniqueId from 'lodash/uniqueId';

export default [
    {
        id: uniqueId(),
        key: 'FNm',
        englishName: 'First name',
        frenchName: 'Prénom'
    },
    {
        id: uniqueId(),
        key: 'LNm',
        englishName: 'Last name',
        frenchName: 'Nom'
    }
]
