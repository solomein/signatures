import fakeAdmins from './fake-admin-admins';

export default {
    id: fakeAdmins[0].id,
    email: 'richard.roe@chryso.com',
    lastName: 'Roe',
    firstName: 'Richard',
    counters: {
        dynamicFields: 15,
        templates: 10,
        signatories: 388,
        users: 5
    }
}
