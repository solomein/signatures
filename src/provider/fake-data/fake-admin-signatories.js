import uniqueId from 'lodash/uniqueId';
import fakeTemplate from './fake-templates';

const data = [
    {
        id: uniqueId(),
        email: 'richard.roe@chryso.com',
        template: fakeTemplate[0],
        lastConnect: Date.now()
    }
];

for (let i = 0; i < 200; i++) {
    data.push({
        id: uniqueId(),
        email: 'richard.roe@chryso.com',
        template: fakeTemplate[0],
        lastConnect: Date.now()
    });
}

export default data;
