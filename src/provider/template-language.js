export default {
    getLanguages() {
        return [
            'english',
            'french'
        ];
    },

    getLanguageFilter() {
        return [
            'any',
            'english',
            'french'
        ];
    }
}
