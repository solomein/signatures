import config from 'config';

export { default as i18n } from './i18n';
export { default as templateLanguage } from './template-language';
export { default as templateStatus } from './template-status';
export { default as templateMultipart } from './template-multipart';
export { default as adminTemplateMultipart } from './admin-template-multipart';
export { default as adminSignatoryMultipart } from './admin-signatory-multipart';

let provider;

if (config.appFakes) {
    provider = require('./fake-provider');
}
else {
    provider = require('./api-provider');
}

export const template = provider.template;
export const user = provider.user;
export const adminTemplate = provider.adminTemplate;
export const adminDynamicField = provider.adminDynamicField;
export const adminSignatory = provider.adminSignatory;
export const file = provider.file;
export const admin = provider.admin;
