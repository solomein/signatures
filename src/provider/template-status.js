export default {
    getStatusFilter() {
        return [
            'any',
            'active',
            'inactive'
        ];
    }
}
