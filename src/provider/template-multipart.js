import templateLanguage from './template-language';

const defaultLang = templateLanguage.getLanguageFilter()[0];

const multipart = {
    getNextPartParams(term, lang) {
        if (term) {
            this.term = term;
        }

        if (lang) {
            this.lang = lang;
        }

        const params = {
            skip: this.expectedCount,
            count: this.partSize,
            term: this.term,
            lang: this.lang
        };

        this.expectedCount += this.partSize;

        return params;
    },

    reset() {
        this.partSize = 5;
        this.expectedCount = 0;
        this.term = '';
        this.lang = defaultLang;

        return this;
    }
};


export default multipart.reset();
