const multipart = {
    getNextPartParams(term) {
        if (term) {
            this.term = term;
        }

        const params = {
            skip: this.expectedCount,
            count: this.partSize,
            term: this.term
        };

        this.expectedCount += this.partSize;

        return params;
    },

    reset() {
        this.partSize = 20;
        this.expectedCount = 0;
        this.term = '';

        return this;
    }
};

export default multipart.reset();
