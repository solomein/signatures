import { get, post, put, del } from 'utils/ajax';

export default {
    getDynamicFieldList() {
        return get('admin/getDynamicFieldList');
    },
    getDynamicField(id) {
        return new Promise(resolve => {
            if (id) {
                get('admin/getDynamicField', { id }).then(resolve);
            }
            else {
                resolve({});
            }
        });
    },
    createDynamicField(data) {
        return post('admin/createDynamicField', data)
    },
    updateDynamicField(data) {
        return put('admin/updateDynamicField', data);
    },
    deleteDynamicField(id) {
        return del('admin/deleteDynamicField', id);
    }
}
