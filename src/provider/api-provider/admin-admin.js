import { get, post, put, del } from 'utils/ajax';

export default {
    login(email, password) {
        return post('admin/login', { email, password });
    },

    logout() {
        return post('admin/logout');

    },

    resetPassword(email) {
        return post('admin/resetPassword', email);
    },

    storeProfile() {
        return get('admin/getCurrentAdminProfile').then(data => {
            this.profile = data;
            return data;
        });
    },

    getStoredProfile() {
        return this.profile;
    },

    getAdminList() {
        return get('admin/getAdminList');
    },

    getAdmin(id) {
        return new Promise(resolve => {
            if (id) {
                get('admin/getAdmin', { id }).then(resolve);
            }
            else {
                resolve({
                    isActive: true
                });
            }
        });
    },
    createAdmin(data) {
        return post('admin/createAdmin', data)
    },
    updateAdmin(data) {
        return put('admin/updateAdmin', data);
    },
    deleteAdmin(id) {
        return del('admin/deleteAdmin', id);
    }
}
