import { get, post } from 'utils/ajax';

export default {
    getTemplateList(data) {
        return get('getTemplateList', data);
    },
    getTemplate(id) {
        return new Promise(resolve => {
            if (id) {
                get('getTemplate', { id }).then(data => {
                    if (!data.fields) {
                        data.fields = []
                    }
                    resolve(data)
                });
            }
            else {
                resolve({ fields: [] });
            }
        });
    },
    setupTemplate(data) {
        return post('setupTemplate', data);
    }
}
