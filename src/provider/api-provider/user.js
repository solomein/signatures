import { get, post } from 'utils/ajax';

export default {
    connect(email) {
        return post('connect', { email });
    },

    storeProfile() {
        return get('getCurrentuserProfile').then(data => {
            this.profile = data;
            return data;
        });
    },

    getStoredProfile() {
        return this.profile;
    }
}
