import { get, put, del } from 'utils/ajax';

export default {
    getSignatoryList(data) {
        return get('admin/getSignatoryList', data);
    },
    getSignatory(id) {
        return new Promise(resolve => {
            if (id) {
                get('admin/getSignatory', { id }).then(resolve);
            }
            else {
                resolve({});
            }
        });
    },
    getSignatoryTemplate(id, signatoryId) {
        return new Promise(resolve => {
            if (id) {
                get('admin/getSignatoryTemplate', { id, signatoryId }).then(data => {
                    if (!data.fields) {
                        data.fields = []
                    }
                    resolve(data)
                });
            }
            else {
                resolve({ fields: [] });
            }
        });
    },
    updateSignatory(data) {
        return put('admin/updateSignatory', data);
    },
    deleteSignatory(id) {
        return del('admin/deleteSignatory', id);
    }
}
