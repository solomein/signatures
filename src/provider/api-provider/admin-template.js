import { get, post, put, del } from 'utils/ajax';

export default {
    getTemplateList(data) {
        return get('admin/getTemplateList', data);
    },
    getTemplate(id) {
        return new Promise(resolve => {
            if (id) {
                get('admin/getTemplate', { id }).then(data => {
                    if (!data.fieldIds) {
                        data.fieldIds = [];
                    }
                    if (data.replacedById === '' || data.replacedById === null) {
                        delete data.replacedById;
                    }
                    resolve(data);
                });
            }
            else {
                resolve({ fieldIds: [] });
            }
        });
    },
    getAllActiveTemplates() {
        return get('admin/getAllActiveTemplates');
    },
    createTemplate(data) {
        return post('admin/createTemplate', data);
    },
    updateTemplate(data) {
        return put('admin/updateTemplate', data);
    },
    deleteTemplate(id) {
        return del('admin/deleteTemplate', id);
    }
}
