import { post, getUrl } from 'utils/ajax';

export default {
    upload(formData) {
        return post('admin/files/upload', () => {
            return new FormData(formData);
        }, {});
    },

    download(id) {
        location.href = getUrl('admin/files/download', { id });
    }
}
