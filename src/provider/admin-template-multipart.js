import templateLanguage from './template-language';
import templateStatus from './template-status';

const defaultLang = templateLanguage.getLanguageFilter()[0];
const defaultStatus = templateStatus.getStatusFilter()[0];

const multipart = {
    getNextPartParams(term, lang, status) {
        if (term) {
            this.term = term;
        }

        if (lang) {
            this.lang = lang;
        }

        if (status) {
            this.status = status;
        }

        const params = {
            skip: this.expectedCount,
            count: this.partSize,
            term: this.term,
            lang: this.lang,
            status: this.status
        };

        this.expectedCount += this.partSize;

        return params;
    },

    reset() {
        this.partSize = 10;
        this.expectedCount = 0;
        this.term = '';
        this.lang = defaultLang;
        this.status = defaultStatus;

        return this;
    }
};

export default multipart.reset();
