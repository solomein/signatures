export default {
    enter: {
        welcome: `Welcome to the automatic email signature tool for the CHRYSO Group.
        Please sign in with your email.`,
        email: 'Email',
        language: 'Language',
        enterButton: 'Enter'
    },
    templateList: {
        myTemplate: 'My signature template',
        otherTemplates: 'Other signature templates',
        templates: 'Signature templates',
        search: 'Search by template name',
        setup: 'Setup',
        select: 'Select'
    },
    templateSetup: {
        send: 'Send me the signature',
        selectAnother: 'Select another signature',
        sendSuccessTitle: 'The email was sent',
        sendSuccessText: 'Thanks for using the Chryso signature service!',
        sendFailureTitle: 'Unable to setup signature',
        sendFailureText: 'Please contact your administrator',
        backButton: 'Back to templates'
    },
    login: {
        email: 'Email',
        password: 'Password',
        language: 'Language',
        enterButton: 'Log in',
        forgotPassword: 'Forgot your password?'
    },
    resetPassword: {
        text: 'Enter your email address and we\'ll send you a reminder of your details along with instructions on how to reset your password.',
        email: 'Email',
        sendButton: 'Send instructions',
        cancelButton: 'Cancel'
    },
    adminHome: {
        fields: 'Dynamic fields',
        templates: 'Signature templates',
        signatories: 'Signatories',
        users: 'Users'
    },
    adminFieldList: {
        addButton: 'Add field',
        saveButton: 'Save',
        deleteButton: 'Delete',
        key: 'Field name',
        englishName: 'English text',
        frenchName: 'French text'
    },
    adminSignatoryEdit: {
        email: 'Email',
        emailPlaceholder: 'Email',
        template: 'Template'
    },
    adminSignatoryOperationMessages: {
        getError: 'Unable to get the signatory',
        deleteSuccess: 'The signatory was successfully deleted',
        deleteError: 'Unable to delete the signatory',
        updateSuccess: 'The signatory was successfully updated',
        updateError: 'Unable to update the signatory'
    },
    adminSignatoryList: {
        email: 'Email',
        templateName: 'Template name',
        language: 'Language',
        lastConnect: 'Last connected',
        search: 'Search by email and template name'
    },
    adminTemplateEdit: {
        frenchName: 'French name',
        frenchNamePlaceholder: 'French name',
        englishName: 'English name',
        englishNamePlaceholder: 'English name',
        isActive: 'Is active',
        language: 'Language',
        templatePackage: 'Template package',
        thumbnail: 'Thumbnail',
        fields: 'Dynamic fields',
        replacedBy: 'Replaced by new tempalte'
    },
    adminTemplateOperationMessages: {
        getError: 'Unable to get the template',
        createSuccess: 'The template was successfully created',
        createError: 'Unable to create the template',
        deleteSuccess: 'The template was successfully deleted',
        deleteError: 'Unable to delete the template',
        updateSuccess: 'The template was successfully updated',
        updateError: 'Unable to update the template'
    },
    adminTemplateList: {
        search: 'Search by template name',
        addButton: 'Add template'
    },
    templateLanguage: {
        any: 'Any language',
        english: 'English',
        french: 'Français'
    },
    templateStatus: {
        any: 'Any status',
        active: 'Active',
        inactive: 'Inactive'
    },
    breadcrumbs: {
        home: 'Home',
        signatories: 'Signatories',
        signatory: 'Edit',
        templates: 'Templates',
        newTemplate: 'New',
        template: 'Edit',
        users: 'Users',
        newUser: 'New',
        user: 'Edit',
        dynamicFields: 'Dynamic fields'
    },
    entityEdit: {
        createButton: 'Create',
        updateButton: 'Update',
        deleteButton: 'Delete',
        cancelButton: 'Cancel',
        confirmDelete: 'Are you sure you want to delete?'
    },
    userEdit: {
        firstName: 'First name',
        firstNamePlaceholder: 'First name',
        lastName: 'Last name',
        lastNamePlaceholder: 'Last name',
        email: 'Email',
        emailPlaceholder: 'Email',
        currentPassword: 'Current password',
        newPassword: 'New password',
        confirmPassword: 'Confirm password',
        password: 'Password',
        passwordPlaceholder: 'Password',
        isActive: 'Is active'
    },
    userOperationMessages: {
        getError: 'Unable to get the user',
        createSuccess: 'The user was successfully created',
        createError: 'Unable to create the user',
        deleteSuccess: 'The user was successfully deleted',
        deleteError: 'Unable to delete the user',
        updateSuccess: 'The user was successfully updated',
        updateError: 'Unable to update the user'
    },
    userList: {
        addButton: 'Add user'
    },
    fileUploader: {
        uploadButton: 'Upload'
    }
}
