export default {
    enter: {
        welcome: `Bienvenue dans l\'outil de CHRYSO Group pour la création des signatures des courriels
        Saisissez votre courriel s\'il vous plaît.`,
        email: 'Courriel',
        language: 'Langue',
        enterButton: 'Entrer'
    },
    templateList: {
        myTemplate: 'Mon modèle de signature',
        otherTemplates: 'Autres modèles de signatures',
        templates: 'Modèles de signatures',
        search: 'Chercher dans les noms des modèles',
        setup: 'Configurer',
        select: 'Sélectionner'
    },
    templateSetup: {
        send: 'Envoyez-moi la signature',
        selectAnother: 'Sélectionner un autre modèle',
        sendSuccessTitle: 'Le courriel avec votre signature vous a été adressé',
        sendSuccessText: 'Merci d\'avoir utilisé l\'outil de création des signatures de CHRYSO Group !',
        sendFailureTitle: 'Impossible de produire la signature',
        sendFailureText: 'Veuillez contacter votre administrateur',
        backButton: 'Retourner vers la liste des modèles'
    },
    login: {
        email: 'Courriel',
        password: 'Mot de passe',
        language: 'Langue',
        enterButton: 'Connexion',
        forgotPassword: 'Mot de passe oublié ?'
    },
    resetPassword: {
        text: 'Saisissez votre courriel et nous vous enverrons les instructions d\'initialisation de votre mot de passe.',
        email: 'Courriel',
        sendButton: 'Envoyer les instructions',
        cancelButton: 'Annuler'
    },
    adminHome: {
        fields: 'Champs dynamiques',
        templates: 'Modèles de signatures',
        signatories: 'Signataires',
        users: 'Utilisateurs'
    },
    adminFieldList: {
        addButton: 'Nouveau champ dynamique',
        saveButton: 'Enregistrer',
        deleteButton: 'Supprimer',
        key: 'Champ',
        englishName: 'Libellé anglais',
        frenchName: 'Libellé français'
    },
    adminSignatoryEdit: {
        email: 'Courriel',
        emailPlaceholder: 'Courriel',
        template: 'Modèle'
    },
    adminSignatoryOperationMessages: {
        getError: 'Impossible de récupérer le signataire',
        deleteSuccess: 'Le signataire a été supprimé',
        deleteError: 'Impossible de supprimer le signataire',
        updateSuccess: 'Le signataire a été mis à jour avec succès',
        updateError: 'Impossible de mettre à jour le signataire'
    },
    adminSignatoryList: {
        email: 'Courriel',
        templateName: 'Modèle de signature',
        language: 'Langue',
        lastConnect: 'Dernière connexion',
        search: 'Chercher par courriel ou nom de modèle'
    },
    adminTemplateEdit: {
        frenchName: 'Nom en français',
        frenchNamePlaceholder: 'Nom en français',
        englishName: 'Nom en anglais',
        englishNamePlaceholder: 'Nom en anglais',
        isActive: 'Est actif',
        language: 'Langue',
        templatePackage: 'Ficher kit du modèle',
        thumbnail: 'Vignette',
        fields: 'Champs dynamiques',
        replacedBy: 'Remplacé par un nouveau modèle'
    },
    adminTemplateOperationMessages: {
        getError: 'Impossible de récupérer le modèle',
        createSuccess: 'Le modèle a été créé',
        createError: 'Impossible de créer le modèle',
        deleteSuccess: 'Le modèle a été supprimé',
        deleteError: 'Impossible de supprimer le modèle',
        updateSuccess: 'Le modèle a été mis à jour',
        updateError: 'Impossible de mettre le modèle'
    },
    adminTemplateList: {
        search: 'Rechercher par le nom de modèle',
        addButton: 'Aouter un modèle'
    },
    templateLanguage: {
        any: 'Toutes les langues',
        english: 'English',
        french: 'Français'
    },
    templateStatus: {
        any: 'Tout statut',
        active: 'Actifs',
        inactive: 'Inactifs'
    },
    breadcrumbs: {
        home: 'Accueil',
        signatories: 'Signataires',
        signatory: 'Modifier',
        templates: 'Modèles',
        newTemplate: 'Nouveau',
        template: 'Modifier',
        users: 'Utilisateurs',
        newUser: 'Nouveau',
        user: 'Modifier',
        dynamicFields: 'Champs dynamiques'
    },
    entityEdit: {
        createButton: 'Créer',
        updateButton: 'Enregistrer',
        deleteButton: 'Supprimer',
        cancelButton: 'Annuler',
        confirmDelete: 'Confirmez-vous la suppression ?'
    },
    userEdit: {
        firstName: 'Prénom',
        firstNamePlaceholder: 'Prénom',
        lastName: 'Nom',
        lastNamePlaceholder: 'Nom',
        email: 'Courriel',
        emailPlaceholder: 'Courriel',
        currentPassword: 'Mot de passe actuel',
        newPassword: 'Nouveau mot de passe',
        confirmPassword: 'Confirmation du nouveau mot de passe',
        password: 'Mot de passe',
        passwordPlaceholder: 'Mot de passe',
        isActive: 'Est actif'
    },
    userOperationMessages: {
        getError: 'Impossible de lire l\'utilisateur',
        createSuccess: 'L\'utilisateur a été créé',
        createError: 'Impossible de créer l\'utilisateur',
        deleteSuccess: 'L\'utilisateur a été supprimé',
        deleteError: 'Impossible de supprimer l\'utilisateur',
        updateSuccess: 'L\'utilisateur a été mis à jour',
        updateError: 'Impossible de mettre à jour l\'utilisateur'
    },
    userList: {
        addButton: 'Nouvel utilisateur'
    },
    fileUploader: {
        uploadButton: 'Télécharger'
    }
}
