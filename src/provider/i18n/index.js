import pick from 'lodash/pick';
import memoize from 'lodash/memoize';

const resources = {
    'en-GB': require('./enGB').default,
    'fr': require('./fr').default
};

const languages = [
    {
        locale: 'en-GB',
        name: 'English'
    },
    {
        locale: 'fr',
        name: 'Français'
    }
];

function getTranslation(locale, keys) {
    return pick(resources[locale], keys);
}

export default {
    getLanguages() {
        return languages;
    },

    getTranslation: memoize(getTranslation, (locale, keys) => {
        return `${locale} ${keys.join(' ')}`;
    })
}
