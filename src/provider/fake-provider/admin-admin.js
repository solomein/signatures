import fakeAdmins from 'provider/fake-data/fake-admin-admins';
import fakeAdminProfile from 'provider/fake-data/fake-admin-profile';
import find from 'lodash/find';
import mockPost from './mock-post';

export default {
    login(name, password) {
        return new Promise((resolve, reject) => {
            resolve();
        });
    },

    logout() {
        return new Promise((resolve) => {
            resolve();
        });
    },

    resetPassword() {
        return new Promise(resolve => {
            resolve();
        });
    },

    storeProfile() {
        return new Promise(resolve => {
            this.profile = fakeAdminProfile;
            resolve(fakeAdminProfile);
        });
    },

    getStoredProfile() {
        return this.profile;
    },

    getAdminList() {
        return new Promise((resolve) => {
            resolve(fakeAdmins);
        });
    },
    getAdmin(id) {
        return new Promise((resolve) => {
            const admin = id
                ? find(fakeAdmins, { id })
                : {
                    isActive: true
                };
            resolve(admin);
        });
    },
    createAdmin: mockPost('create admin'),
    updateAdmin: mockPost('update admin'),
    deleteAdmin: mockPost('delete admin')
}
