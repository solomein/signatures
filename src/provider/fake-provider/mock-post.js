/*eslint no-console:0*/

export default function mockPost(msg, action) {
    return function(data) {
        console.log(msg, data);
        return new Promise((resolve, reject) => {
            if (action) {
                action(data);
            }

            setTimeout(() => {
                resolve(data);
            }, 300);
        });
    }
}
