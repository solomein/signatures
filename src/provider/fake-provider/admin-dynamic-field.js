import fakeAdminDynamicFields from 'provider/fake-data/fake-admin-dynamic-fields';
import find from 'lodash/find';
import mockPost from './mock-post';

export default {
    getDynamicFieldList() {
        return new Promise((resolve) => {
            resolve(fakeAdminDynamicFields);
        });
    },
    getDynamicField(key) {
        return new Promise((resolve) => {
            const field = key ? find(fakeAdminDynamicFields, { key }) : {};
            resolve(field);
        });
    },
    createDynamicField: mockPost('create dynamic field'),
    updateDynamicField: mockPost('update dynamic field'),
    deleteDynamicField: mockPost('delete dynamic field')
}
