import fakeAdminSignatories from 'provider/fake-data/fake-admin-signatories';
import fakeTemplates from 'provider/fake-data/fake-templates';
import find from 'lodash/find';
import mockPost from './mock-post';

export default {
    getSignatoryList(data) {
        return new Promise((resolve) => {
            resolve(fakeAdminSignatories.slice(data.skip, data.skip + data.count));
        });
    },
    getSignatory(id) {
        return new Promise((resolve) => {
            const signatory = id ? find(fakeAdminSignatories, { id }) : {};
            resolve(signatory);
        });
    },
    getSignatoryTemplate(id, signatoryId) {
        return new Promise((resolve) => {
            const template = id ? find(fakeTemplates, { id }) : {};
            resolve(template || fakeTemplates[0]);
        });
    },
    updateSignatory: mockPost('update signatory'),
    deleteSignatory: mockPost('delete signatory')
}
