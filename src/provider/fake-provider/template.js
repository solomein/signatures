import fakeTemplates from 'provider/fake-data/fake-templates';
import mockPost from './mock-post';
import find from 'lodash/find';

export default {
    getTemplateList(data) {
        return new Promise((resolve) => {
            resolve(fakeTemplates.slice(data.skip, data.skip + data.count));
        });
    },
    getTemplate(id) {
        return new Promise((resolve) => {
            const template = id ? find(fakeTemplates, { id }) : {};
            resolve(template || fakeTemplates[0]);
        });
    },
    setupTemplate: mockPost('setup template')
}
