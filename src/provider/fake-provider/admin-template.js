import fakeAdminTemplates from 'provider/fake-data/fake-admin-templates';
import find from 'lodash/find';
import mockPost from './mock-post';

export default {
    getTemplateList(data) {
        return new Promise((resolve) => {
            resolve(fakeAdminTemplates.slice(data.skip, data.skip + data.count));
        });
    },
    getTemplate(id) {
        return new Promise((resolve) => {
            const template = id ? find(fakeAdminTemplates, { id }) : {};
            if (!template.fieldIds) {
                template.fieldIds = [];
            }
            if (template.replacedById === '' || template.replacedById === null) {
                delete template.replacedById;
            }
            resolve(template);
        });
    },
    getAllActiveTemplates() {
        return new Promise((resolve) => {
            resolve(fakeAdminTemplates.filter(x => x.isActive).map(x => ({
                id: x.id,
                englishName: x.englishName,
                frenchName: x.frenchName
            })));
        })
    },
    createTemplate: mockPost('create template'),
    updateTemplate: mockPost('update template'),
    deleteTemplate: mockPost('delete template')
}
