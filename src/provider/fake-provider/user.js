import fakeUserProfile from 'provider/fake-data/fake-user-profile';
// import mockPost from './mock-post';
// import find from 'lodash/find';

export default {
    connect(email) {
        return new Promise(resolve => {
            resolve(email);
        });
    },

    storeProfile() {
        return new Promise(resolve => {
            this.profile = fakeUserProfile;
            resolve(fakeUserProfile);
        });
    },

    getStoredProfile() {
        return this.profile;
    }
}
