import React, { Component, PropTypes } from 'react';
import SendSuccess from 'dumb/send-success';
import SendFailure from 'dumb/send-failure';
import { connect } from 'react-redux';
import SendIcon from 'material-ui/svg-icons/content/send';
import LoopIcon from 'material-ui/svg-icons/av/loop';
import FlatButton from 'material-ui/FlatButton';
import { TemplateImage } from 'dumb/template-tile';
import Card from 'dumb/card';
import { Form, FormGroup } from 'dumb/form';
import { TextField } from 'dumb/text-field';
import { index } from 'nav';
import { Content } from 'dumb/layout';
import Divider from 'material-ui/Divider';
import { getTemplate, resetTemplate, setupTemplate } from 'actions/template-action-creators';
import { getAndStoreUserProfile } from 'actions/user-action-creators';
import { getTemplateName, getFieldName } from 'i18n/helpers';
import translate from 'i18n/translate';

const { object, func, string } = PropTypes;

const stateToProps = state => ({
    template: state.get('template')
});

const actionsToProps = dispatch => ({
    getTemplate: templateId => dispatch(getTemplate(templateId)),
    resetTemplate: () => dispatch(resetTemplate()),
    setupTemplate: data => dispatch(setupTemplate(data)),
    getUserProfile: () => dispatch(getAndStoreUserProfile()),
    dispatch
});

@connect(stateToProps, actionsToProps)
@translate('templateSetup')
export default class TemplateConfiguration extends Component {
    static contextTypes = {
        router: object
    };

    static propTypes = {
        getTemplate: func,
        resetTemplate: func,
        params: object,
        template: object,
        setupTemplate: func,
        locale: string,
        translation: object,
        getUserProfile: func
    };

    constructor(props) {
        super(props);
        this.state = {
            data: props.template.get('data')
        };
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.template.get('sended')) {
            this.props.getUserProfile();
        }
        if (nextProps.template !== this.state.data) {
            this.setState({ data: nextProps.template.get('data') });
        }
    }

    componentWillMount() {
        this.props.getTemplate(this.props.params.templateId);
    }

    componentWillUnmount() {
        this.props.resetTemplate();
    }

    shouldComponentUpdate(nextProps) {
        return nextProps.template !== this.props.template;
    }

    render() {
        if (this.props.template.get('error')) {
            return <Content offset={2}>
                <SendFailure onBack={() => this.handleBack()}/>
            </Content>;
        }

        if (this.props.template.get('sended')) {
            return <Content offset={2}>
                <SendSuccess onBack={() => this.handleBack()}/>
            </Content>;
        }

        const data = this.state.data;

        if (data === null) {
            return null;
        }

        const sending = this.props.template.get('sending');

        return <Content offset={1}>
            <Card>
                <TemplateImage
                    imageSrc={data.get('thumb')}
                    focused={true}
                />
                <Divider style={{marginTop: 0}}/>
                <Form>
                    <FormGroup>
                        {getTemplateName(this.props.locale, data)}
                    </FormGroup>
                    {data.get('fields') && data.get('fields').map((field, i) => {
                        return <FormGroup key={field.get('key')}>
                            <TextField
                                value={this.state.data.getIn(['fields', i, 'value'])}
                                label={getFieldName(this.props.locale, field)}
                                placeholder={getFieldName(this.props.locale, field)}
                                onChange={val => this.handleFieldChange(i, val)}
                                disabled={sending}
                            />
                        </FormGroup>
                    })}
                    <div style={{marginTop: 36}}>
                        <FlatButton
                            label={this.props.translation.templateSetup.send}
                            icon={<SendIcon />}
                            onTouchTap={() => this.handleSend()}
                            disabled={sending}
                        />
                        <FlatButton
                            label={this.props.translation.templateSetup.selectAnother}
                            icon={<LoopIcon />}
                            onTouchTap={() => this.handleBack()}
                            disabled={sending}
                        />
                    </div>
                </Form>
            </Card>
        </Content>
    }

    handleBack() {
        this.context.router.push(index());
    }

    handleFieldChange(index, value) {
        this.setState({
            data: this.state.data.setIn(['fields', index, 'value'], value)
        });
    }

    handleSend() {
        this.props.setupTemplate(this.state.data.toJS());
    }
}
