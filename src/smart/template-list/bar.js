import React, { Component, PropTypes } from 'react';
import { TextField } from 'dumb/text-field';
import Dropdown, { getTranslation } from 'dumb/dropdown';
import { Bar } from 'dumb/layout';
import AppBar, { AppBarCell, AppBarControlWrap } from 'dumb/app-bar';
import SearchIcon from 'material-ui/svg-icons/action/search';
import { templateLanguage } from 'provider';
import debounce from 'lodash/debounce';
import translate from 'i18n/translate';

const { func, object } = PropTypes;

@translate('templateLanguage', 'templateList')
export default class SearchBar extends Component {
    static propTypes = {
        onChange: func,
        translation: object
    };

    constructor(props) {
        super(props);
        this.state = {
            lang: templateLanguage.getLanguageFilter()[0],
            term: ''
        };

        this.callChange = debounce(data => this.props.onChange(data), 250);
    }

    shouldComponentUpdate(nextProps, nextState) {
        return nextState.lang !== this.state.lang
            || nextState.term !== this.state.term;
    }

    render() {
        const langs = getTranslation(
            templateLanguage.getLanguageFilter(),
            this.props.translation.templateLanguage
        );

        return <Bar offset={1}>
            <AppBar second={true}>
                <AppBarCell>
                    <AppBarControlWrap>
                        <SearchIcon />
                    </AppBarControlWrap>
                    <AppBarControlWrap style={{ width: 440, height: 41 }}>
                        <TextField
                            placeholder={this.props.translation.templateList.search}
                            onChange={val => this.handleTermChange(val)}
                            value={this.state.term}
                        />
                    </AppBarControlWrap>
                    <AppBarControlWrap>
                        <Dropdown
                            value={this.state.lang}
                            data={langs}
                            onChange={val => this.handleLangChange(val)}
                        />
                    </AppBarControlWrap>
                </AppBarCell>
            </AppBar>
        </Bar>;
    }

    handleLangChange(lang) {
        const data = Object.assign({}, this.state, { lang });
        this.setState(data);
        this.callChange(data);
    }

    handleTermChange(term) {
        const data = Object.assign({}, this.state, { term });
        this.setState(data);
        this.callChange(data);
    }
}
