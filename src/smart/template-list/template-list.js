import React, { Component, PropTypes } from 'react';
import css from './template-list.css';
import TemplateTile from 'dumb/template-tile';
import { template } from 'nav';
import { Content } from 'dumb/layout';
import ScrollContainer from 'dumb/scroll-container';
import { connect } from 'react-redux';
import { getTemplateList, resetTemplateList, getTemplateListPart } from 'actions/template-action-creators';
import { getCurrentTemplate } from 'actions/user-action-creators';
import SearchBar from './bar';
import { getTemplateName } from 'i18n/helpers';
import translate from 'i18n/translate';
import TuneIcon from 'material-ui/svg-icons/image/tune';
import CheckIcon from 'material-ui/svg-icons/av/playlist-add-check';
import FlatButton from 'material-ui/FlatButton';

const { object, func, string } = PropTypes;

const stateToProps = state => ({
    list: state.get('baseList'),
    user: state.get('user')
});

const actionsToProps = dispatch => ({
    getTemplateList: (term, lang) => dispatch(getTemplateList(term, lang)),
    getTemplateListPart: (term, lang) => dispatch(getTemplateListPart(term, lang)),
    resetTemplateList: () => dispatch(resetTemplateList()),
    getUserTemplate: templateId => dispatch(getCurrentTemplate(templateId)),
    dispatch
});

@connect(stateToProps, actionsToProps)
@translate('templateList')
export default class TemplateList extends Component {
    static contextTypes = {
        router: object
    };

    static propTypes = {
        getTemplateList: func,
        getTemplateListPart: func,
        getUserTemplate: func,
        resetTemplateList: func,
        user: object,
        list: object,
        translation: object,
        locale: string
    };

    componentWillMount() {
        this.props.getTemplateListPart();
        this.props.getUserTemplate(this.props.user.get('templateId'));
    }

    componentWillUnmount() {
        this.props.resetTemplateList();
    }

    shouldComponentUpdate(nextProps) {
        return nextProps.list !== this.props.list
            || nextProps.user !== this.props.user;
    }

    render() {
        return <ScrollContainer onBounce={() => this.handleBounce()}>
            {this.renderBar()}
            <Content offset={2}>
                {this.renderUserTemplate()}
                {this.renderList()}
            </Content>
        </ScrollContainer>;
    }

    renderUserTemplate() {
        if (this.myTemplateIsEmpty()) {
            return null;
        }

        const template = this.props.user.get('template');

        return <div style={{marginBottom: 72}}>
            <div className={css.title}>
                {this.props.translation.templateList.myTemplate}
            </div>
            <TemplateTile
                title={getTemplateName(this.props.locale, template)}
                button={<FlatButton
                    label={this.props.translation.templateList.setup}
                    icon={<TuneIcon />}
                    onTouchTap={() => this.handleSelect(template.get('id'))}
                />}
                image={template.get('thumb')}
                focused={true}
            />
        </div>;
    }

    renderList() {
        const { list } = this.props;

        if (list.get('items') === null) {
            return null;
        }

        const {
            otherTemplates,
            templates
        } = this.props.translation.templateList

        return <div>
            <div className={css.title}>
                {this.myTemplateIsEmpty() ? templates : otherTemplates}
            </div>
            {list.get('items').map(item => <TemplateTile
                key={item.get('id')}
                title={getTemplateName(this.props.locale, item)}
                image={item.get('thumb')}
                button={<FlatButton
                    label={this.props.translation.templateList.select}
                    icon={<CheckIcon />}
                    onTouchTap={() => this.handleSelect(item.get('id'))}
                />}
            />)}
        </div>;
    }

    renderBar() {
        return <SearchBar
            onChange={data => this.handleSearch(data)}
        />;
    }

    myTemplateIsEmpty() {
        return !this.props.user.getIn(['template', 'id']);
    }

    handleSelect(id) {
        this.context.router.push(template(id));
    }

    handleBounce() {
        if (this.props.list.get('isCompleted')) {
            return;
        }
        this.props.getTemplateListPart();
    }

    handleSearch(data) {
        this.props.getTemplateList(data.term, data.lang);
    }
}
