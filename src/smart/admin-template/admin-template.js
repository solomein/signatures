import React, { PropTypes } from 'react';
import { adminTemplates } from 'nav';
import TemplateEdit from 'dumb/template-edit';
import { connect } from 'react-redux';
import {
    getTemplate,
    resetTemplate,
    updateTemplate,
    createTemplate,
    deleteTemplate
} from 'actions/admin-template-action-creators';
import BaseEdit from 'smart/base/base-entity-edit';
import translate from 'i18n/translate';
import { Content } from 'dumb/layout';

const stateToProps = state => ({
    entity: state.get('baseEntity')
});

const actionsToProps = dispatch => ({
    getEntity: (templateId) => dispatch(getTemplate(templateId)),
    resetEntity: () => dispatch(resetTemplate()),
    updateEntity: data => dispatch(updateTemplate(data)),
    createEntity: data => dispatch(createTemplate(data)),
    deleteEntity: data => dispatch(deleteTemplate(data)),
    dispatch
});

@connect(stateToProps, actionsToProps)
@translate('breadcrumbs', 'adminTemplateOperationMessages')
export default class Template extends BaseEdit {
    static contextTypes = {
        router: PropTypes.object.isRequired
    };

    behavior = {
        create: {
            redirect: () => adminTemplates()
        },
        delete: {
            redirect: () => adminTemplates()
        },
        update: {
            redirect: () => adminTemplates()
        }
    };

    operationMessages = () => this.props.translation.adminTemplateOperationMessages;

    fetchEntity(p) {
        this.props.getEntity(p.templateId);
    }

    componentWillUnmount() {
        this.props.resetEntity();
    }

    getBreadcrumbs(crumb) {
        return [
            crumb.home(),
            crumb.templates(),
            this.isNew() ? crumb.newTemplate() : crumb.template()
        ];
    }

    isNew() {
        return !this.props.params.templateId;
    }

    renderMainForm() {
        return <Content offset={1}>
            <div style={{width: 540}}>
                <TemplateEdit
                    data={this.props.entity.get('data')}
                    languages={this.props.entity.getIn(['associated', 'languages'])}
                    fields={this.props.entity.getIn(['associated', 'fields'])}
                    templates={this.props.entity.getIn(['associated', 'templates'])}
                    onSave={data => this.handleSave(data)}
                    onDelete={data => this.handleDelete(data)}
                />
            </div>
        </Content>;
    }

    handleSave(data) {
        if (this.isNew()) {
            this.props.createEntity(data.toJS());
        }
        else {
            this.props.updateEntity(data.toJS());
        }
    }

    handleDelete(data) {
        this.props.deleteEntity(data);
    }
}
