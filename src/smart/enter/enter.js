import React, { Component, PropTypes } from 'react';
import EnterForm from 'dumb/enter-form';
import { connect as connectUser, getLastEmail, getLastLocale } from 'user-connect';
import Centring from 'dumb/centring';
import Snackbar from 'dumb/snackbar';
import { index } from 'nav';
import { i18n } from 'provider';
import { connect } from 'react-redux';
import { setUserLocale } from 'actions/user-action-creators';

const { object } = PropTypes;

@connect()
export default class Enter extends Component {
    static childContextTypes = {
        area: PropTypes.string
    };

    getChildContext() {
        return {
            area: 'user'
        };
    }

    static contextTypes = {
        router: object
    };

    static propTypes = {
        location: object
    };

    constructor(props) {
        super(props);
        this.state = { error: false, request: false };
    }

    componentWillMount() {
        this.props.dispatch(setUserLocale(getLastLocale()));
    }

    render() {
        return (
            <Centring>
                <EnterForm
                    email={getLastEmail() || ''}
                    onSubmit={(email, locale) => this.handleSubmit(email, locale)}
                    languages={i18n.getLanguages()}
                    onChangeLocale={val => this.handleChangeLocale(val)}
                    defaultLocale={getLastLocale()}
                    disabled={this.state.request}
                />
                <Snackbar
                    open={this.state.error}
                    message="Login error"
                />
            </Centring>
        );
    }

    handleSubmit(email, locale) {
        const { location } = this.props;
        const { router } = this.context;

        this.setState({
            request: true
        });
        connectUser(email, locale).then(() => {
            if (location.state && location.state.nextPathname) {
                router.replace(location.state.nextPathname);
            }
            else {
                router.replace(index());
            }
        })
        .catch(() => {
            this.setState({ error: true, request: false });
        });
    }

    handleChangeLocale(locale) {
        this.props.dispatch(setUserLocale(locale));
    }
}
