import React, { Component, PropTypes } from 'react';
import { admin } from 'provider';
import Centring from 'dumb/centring';
import { adminLogin } from 'nav';
import ResetPasswordForm from 'dumb/reset-password';

const { object } = PropTypes;

export default class ResetPassword extends Component {
    static childContextTypes = {
        area: PropTypes.string
    };

    getChildContext() {
        return {
            area: 'admin'
        };
    }

    static contextTypes = {
        router: object
    };

    static propTypes = {
        location: object
    };

    render() {
        return (
            <Centring>
                <ResetPasswordForm
                    onSubmit={this.handleSubmit.bind(this)}
                    onCancel={this.handleCancel.bind(this)}
                />
            </Centring>
        );
    }

    handleSubmit(email) {
        admin.resetPassword(email).then(() => {
            this.context.router.push(adminLogin());
        });
    }

    handleCancel() {
        this.context.router.push(adminLogin());
    }
}
