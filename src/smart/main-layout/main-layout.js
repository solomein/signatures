import React, { Component, PropTypes } from 'react';
import AppBar, { Title, Breadcrumbs, AppBarCell } from 'dumb/app-bar';
import css from './main-layout.css';
import { removeMessage } from 'actions/main-layout-action-creators';
import Snackbar from 'dumb/snackbar';

const { object, element, func } = PropTypes;

export default class MainLayout extends Component {
    static contextTypes = {
        router: object
    };

    static propTypes = {
        mainLayout: object,
        children: element,
        dispatch: func,
        getUserProfile: func,
        user: object
    };

    render() {
        return (
            <section className={css.root}>
                <div className={css.bar} ref="root">
                    <AppBar>
                        <AppBarCell width="1%">
                        <Title collapsed={false}>
                            Chryso
                        </Title>
                        </AppBarCell>
                        <AppBarCell width="1%">
                            <div className={css.breadcrumbs}>
                                {this.renderBreadcrumbs()}
                            </div>
                        </AppBarCell>
                        <AppBarCell>
                            <div className={css.controls}>
                                {this.renderControls && this.renderControls()}
                            </div>
                        </AppBarCell>
                        <AppBarCell width="1%">
                            {this.renderUser()}
                        </AppBarCell>
                    </AppBar>
                </div>
                {this.props.children}
                {this.renderSnackbar()}
            </section>
        );
    }

    renderBreadcrumbs() {
        let crumbs = this.props.mainLayout.get('breadcrumbs');
        if (!crumbs.size) {
            return;
        }

        return <Breadcrumbs
            items={crumbs}
            onChange={url => this.handleCrumbsClick(url)}
        />;
    }

    renderSnackbar() {
        const message = this.props.mainLayout.getIn(['messages', 0]);
        const text = message && message.text;
        const stamp = message && message.stamp;

        return <Snackbar
            open={Boolean(text)}
            message={text}
            onClose={() => this.removeMessage(stamp)}
        />;
    }

    handleCrumbsClick(url) {
        this.context.router.push(url);
    }

    removeMessage(stamp) {
        this.props.dispatch(removeMessage(stamp));
    }
}
