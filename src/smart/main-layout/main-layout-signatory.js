import React, { PropTypes } from 'react';
import MainLayout from './main-layout';
import { connect } from 'react-redux';
import css from './main-layout.css';
import { enter } from 'nav';
import { getUserProfile } from 'actions/user-action-creators';
import { setBreadcrumbs } from 'actions/main-layout-action-creators';
// import { getLastLocale } from 'user-connect';

const stateToProps = state => ({
    mainLayout: state.get('mainLayout'),
    user: state.get('user')
});

const actionsToProps = dispatch => ({
    getUserProfile: () => dispatch(getUserProfile()),
    setBreadcrumbs: () => dispatch(setBreadcrumbs([])),
    // setLocale: () => dispatch(setUserLocale(getLastLocale())),
    dispatch
});

@connect(stateToProps, actionsToProps)
export default class MainLayoutSignatory extends MainLayout {
    static childContextTypes = {
        area: PropTypes.string
    };

    static contextTypes = {
        router: PropTypes.object.isRequired
    };

    getChildContext() {
        return {
            area: 'user'
        };
    }

    componentWillMount() {
        this.props.getUserProfile();
        this.props.setBreadcrumbs();
        // this.props.setLocale();
    }

    renderUser() {
        return <div
            onTouchTap={() => this.handleUserTap()}
            style={{cursor: 'pointer', lineHeight: 1}}
        >
            <div className={css.userId}>
                {this.props.user.get('email')}
            </div>
        </div>
    }

    handleUserTap() {
        this.context.router.push(enter());
    }
}
