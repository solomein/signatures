import React, { PropTypes } from 'react';
import MainLayout from './main-layout';
import ExitIcon from 'material-ui/svg-icons/action/exit-to-app';
import IconButton from 'material-ui/IconButton';
import css from './main-layout.css';
import { adminLogin } from 'nav';
import { connect } from 'react-redux';
import { getAdminProfile } from 'actions/admin-action-creators';
// import { getLastLocale } from 'admin-auth';
import { logout } from 'admin-auth';
import FlatButton from 'material-ui/FlatButton';
import AddIcon from 'material-ui/svg-icons/content/add';
import {
    adminTemplates,
    adminNewTemplate,
    adminUsers,
    adminNewUser,
    adminFields
} from 'nav';
import {
    addDynamicField
} from 'actions/admin-dynamic-field-action-creators';
import translate from 'i18n/translate';

const stateToProps = state => ({
    mainLayout: state.get('mainLayout'),
    admin: state.get('admin')
});

const actionsToProps = dispatch => ({
    getAdminProfile: () => dispatch(getAdminProfile()),
    // setLocale: () => dispatch(setAdminLocale(getLastLocale())),
    dispatch
});

@connect(stateToProps, actionsToProps)
@translate('adminTemplateList', 'adminFieldList', 'userList')
export default class MainLayoutAdmin extends MainLayout {
    static childContextTypes = {
        area: PropTypes.string
    };

    static contextTypes = {
        router: PropTypes.object.isRequired
    };

    getChildContext() {
        return {
            area: 'admin'
        };
    }

    componentWillMount() {
        this.props.getAdminProfile();
        // this.props.setLocale();
    }

    renderUser() {
        return <div>
            <div className={css.userId}>
                {this.props.admin.get('firstName')} {this.props.admin.get('lastName')}
            </div>
            <IconButton
                className={css.actionIcon}
                onTouchTap={() => this.handleExitTap()}
            >
                <ExitIcon color="#fff"/>
            </IconButton>
        </div>;
    }

    renderAddButton(label, handler) {
        return <FlatButton
            label={label}
            backgroundColor="rgba(255, 255, 255, .15)"
            hoverColor="rgba(255, 255, 255, .25)"
            labelStyle={{ color: '#fff' }}
            icon={<AddIcon color="#fff" />}
            rippleColor="#fff"
            style={{ color: '#fff' }}
            onTouchTap={handler}
        />;
    }

    renderControls() {
        const { isActive, push } = this.context.router;
        const t = this.props.translation;

        if (isActive(adminTemplates())) {
            return this.renderAddButton(
                t.adminTemplateList.addButton,
                () => push(adminNewTemplate())
            );
        }

        if (isActive(adminUsers())) {
            return this.renderAddButton(
                t.userList.addButton,
                () => push(adminNewUser())
            );
        }

        if (isActive(adminFields())) {
            return this.renderAddButton(
                t.adminFieldList.addButton,
                () => this.props.dispatch(addDynamicField())
            );
        }
    }

    handleExitTap() {
        logout().then(() => {
            this.context.router.replace(adminLogin());
        });
    }
}
