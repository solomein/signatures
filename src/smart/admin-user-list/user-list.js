import React, { Component, PropTypes } from 'react';
import Card from 'dumb/card';
import { connect } from 'react-redux';
import translate from 'i18n/translate';
import Base from 'smart/base/base';
import { Content } from 'dumb/layout';
import { List } from 'dumb/list';
import BulbIcon from 'material-ui/svg-icons/action/lightbulb-outline';
import { adminUser } from 'nav';
import { getAdminList, resetAdminList } from 'actions/admin-admin-action-creators';

const stateToProps = state => ({
    list: state.get('baseList')
});

const actionsToProps = dispatch => ({
    getList: () => dispatch(getAdminList()),
    resetList: () => dispatch(resetAdminList()),
    dispatch
});

@connect(stateToProps, actionsToProps)
@translate('breadcrumbs')
export default class UserList extends Base {
    static contextTypes = {
        router: PropTypes.object.isRequired
    };

    getBreadcrumbs(crumb) {
        return [
            crumb.home(),
            crumb.users()
        ];
    }

    fetchData() {
        this.props.getList();
    }

    componentWillUnmount() {
        this.props.resetList();
    }

    shouldComponentUpdate(nextProps) {
        return nextProps.list !== this.props.list;
    }

    render() {
        const { list } = this.props;

        if (list.get('items') === null) {
            return null;
        }

        return <Content offset={1}>
            <div style={{ width: 540 }}>
                <Card>
                    <List
                        onSelect={data => this.pushPath(adminUser(data.id))}
                        data={list.get('items')}
                        dataConverter={item => {
                            const iconColor = item.get('isActive') ? '#4CAF50' : '#BDBDBD';
                            const opacity = item.get('isActive') ? 1 : 0.45;
                            const controls = [
                                <div style={{ marginRight: 8 }}>
                                    <BulbIcon color={iconColor}/>
                                </div>
                            ];
                            const name = `${item.get('firstName')} ${item.get('lastName')}`;

                            return {
                                id: item.get('id'),
                                primaryText: <span style={{ opacity }}>{name}</span>,
                                secondaryText: <span style={{ opacity }}>{item.get('email')}</span>,
                                controls
                            };
                        }}
                    />
                </Card>
            </div>
        </Content>;
    }
}
