import React, { PropTypes } from 'react';
import { adminSignatories, adminTemplates, adminFields, adminUsers } from 'nav';
import ContactIcon from 'material-ui/svg-icons/action/account-box';
import PeopleIcon from 'material-ui/svg-icons/social/people';
import ArtTrackIcon from 'material-ui/svg-icons/av/art-track';
import TextFieldsIcon from 'material-ui/svg-icons/editor/text-fields';
import Tile, { TileContainer } from 'dumb/tile';
import { connect } from 'react-redux';
import Base from 'smart/base/base';
import translate from 'i18n/translate';
import { Content } from 'dumb/layout';
import { getAndStoreAdminProfile } from 'actions/admin-action-creators';

const stateToProps = state => ({
    counters: state.getIn(['admin', 'counters']),
    admin: state.getIn(['admin'])
});

const actionsToProps = dispatch => ({
    getAdminProfile: () => dispatch(getAndStoreAdminProfile()),
    dispatch
});

@connect(stateToProps, actionsToProps)
@translate('breadcrumbs', 'adminHome')
export default class SmartHome extends Base {
    static contextTypes = {
        router: PropTypes.object.isRequired
    };

    getBreadcrumbs(crumb) {
        return [
            crumb.home()
        ];
    }

    fetchData() {
        this.props.getAdminProfile();
    }

    render() {
        const {
            fields,
            templates,
            signatories,
            users
        } = this.props.translation.adminHome;
        return <Content offset={1}>
            <div style={{width: 600}}>
                <TileContainer>
                    <Tile
                        title={fields}
                        count={this.props.counters.get('dynamicFields')}
                        iconClass={TextFieldsIcon}
                        onClick={() => this.pushPath(adminFields())}
                    />
                    <Tile
                        title={templates}
                        count={this.props.counters.get('templates')}
                        iconClass={ArtTrackIcon}
                        onClick={() => this.pushPath(adminTemplates())}
                    />
                    <Tile
                        title={signatories}
                        count={this.props.counters.get('signatories')}
                        iconClass={ContactIcon}
                        onClick={() => this.pushPath(adminSignatories())}
                    />
                    <Tile
                        title={users}
                        count={this.props.counters.get('users')}
                        iconClass={PeopleIcon}
                        onClick={() => this.pushPath(adminUsers())}
                    />
                </TileContainer>
            </div>
        </Content>;
    }
}
