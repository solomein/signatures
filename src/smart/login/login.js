import React, { Component, PropTypes } from 'react';
import LoginForm from 'dumb/login-form';
import { login, getLastLocale } from 'admin-auth';
import Snackbar from 'dumb/snackbar';
import { adminResetPassword, adminIndex } from 'nav';
import { connect } from 'react-redux';
import { setAdminLocale } from 'actions/admin-action-creators';
import { i18n } from 'provider';
import Centring from 'dumb/centring';

const { object } = PropTypes;

@connect()
export default class Login extends Component {
    static childContextTypes = {
        area: PropTypes.string
    };

    getChildContext() {
        return {
            area: 'admin'
        };
    }

    static contextTypes = {
        router: object
    };

    static propTypes = {
        location: object
    };

    constructor(props) {
        super(props);
        this.state = { error: false, request: false };
    }

    componentWillMount() {
        this.props.dispatch(setAdminLocale(getLastLocale()));
    }

    render() {
        return (
            <Centring>
                <LoginForm
                    onSubmit={this.handleSubmit.bind(this)}
                    onForgotClick={this.handleForgotClick.bind(this)}
                    languages={i18n.getLanguages()}
                    onChangeLocale={val => this.handleChangeLocale(val)}
                    defaultLocale={getLastLocale()}
                    disabled={this.state.request}
                />
                <Snackbar
                    open={this.state.error}
                    message="Login error"
                />
            </Centring>
        );
    }

    handleChangeLocale(locale) {
        this.props.dispatch(setAdminLocale(locale));
    }

    handleSubmit(email, password, locale) {
        const { location } = this.props;
        const { router } = this.context;

        this.setState({
            request: true
        });
        login(email, password, locale).then(() => {
            if (location.state && location.state.nextPathname) {
                router.replace(location.state.nextPathname);
            }
            else {
                router.replace(adminIndex());
            }
        })
        .catch(() => {
            this.setState({ error: true, request: false });
        });
    }

    handleForgotClick() {
        this.context.router.push(adminResetPassword());
    }
}
