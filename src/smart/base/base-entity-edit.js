import React, { Component, PropTypes } from 'react';
import { setBreadcrumbs, showMessage } from 'actions/main-layout-action-creators';
import css from './base-entity.css';
import { getCrumbs } from './breadcrumbs-helper';
import { index } from 'nav';

const { object, func } = PropTypes;

export default class BaseEntityEdit extends Component {
    static contextTypes = {
        router: object.isRequired
    };

    static propTypes = {
        dispatch: func,
        entity: object,
        params: object,
        getEntity: func,
        translation: object
    };

    componentWillMount() {
        if (this.checkViewPermissions() === false) {
            this.pushPath(index());
            return;
        }

        this.fetchEntity(this.props.params);
    }

    shouldComponentUpdate(nextProps) {
        return nextProps.entity !== this.props.entity;
    }

    componentDidUpdate() {
        const data = this.props.entity.get('data');
        const parents = this.props.entity.get('parents');

        if (data === null || this.crumbsDefined) {
            return;
        }

        this.crumbsDefined = true;
        const crumb = getCrumbs(this.props.translation.breadcrumbs);
        this.setBreadcrumbs(this.getBreadcrumbs(crumb, data, parents));
    }

    componentWillReceiveProps(nextProps) {
        const operation = nextProps.entity.get('lastOperation');
        const operationBehavior = this.behavior[operation];

        const error = nextProps.entity.get('error');
        const errorMsg = this.getOperationErrorMsg(operation);

        if (error) {
            this.props.dispatch(showMessage(errorMsg));
            return;
        }

        if (!nextProps.entity.get('operationSucceeded')) {
            return;
        }

        if (operationBehavior && operationBehavior.onSuccess) {
            operationBehavior.onSuccess();
        }

        if (operationBehavior && operationBehavior.redirect) {
            this.pushPath(operationBehavior.redirect(this.props.params));
        }

        const successMsg = this.getOperationSuccessMsg(operation);

        if (successMsg) {
            this.props.dispatch(showMessage(successMsg));
        }
    }

    getOperationSuccessMsg(operation) {
        const msgs = this.operationMessages();
        return msgs && msgs[`${operation}Success`];
    }

    getOperationErrorMsg(operation) {
        const msgs = this.operationMessages();
        return msgs && msgs[`${operation}Error`];
    }

    render() {
        const { entity } = this.props;
        const loading = entity.get('fetching')
            || entity.get('sending')
            || entity.get('data') === null;

        if (loading) {
            return null;
        }

        return this.renderMainForm();
    }

    pushPath(path) {
        this.context.router.push(path);
    }

    setBreadcrumbs(crumbs) {
        this.props.dispatch(setBreadcrumbs(crumbs));
    }

    checkViewPermissions() {}
}
