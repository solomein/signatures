import React, { Component, PropTypes } from 'react';
import FlatButton from 'material-ui/FlatButton';
import { FormActionGroup } from 'dumb/form';
import Card from 'dumb/card';
import ModeEdit from 'material-ui/svg-icons/editor/mode-edit';
import { setBreadcrumbs } from 'actions/main-layout-action-creators';
import css from './base-entity.css';
import { getCrumbs } from './breadcrumbs-helper';
import Text from 'dumb/text';
import dateFormatter from 'i18n/date-formatter';
import { index } from 'nav';

const { object, func, string } = PropTypes;

export default class BaseEntityView extends Component {
    static contextTypes = {
        router: object
    };

    static propTypes = {
        dispatch: func,
        entity: object,
        params: object,
        getEntity: func,
        translation: object,
        locale: string
    };

    componentWillMount() {
        if (this.checkViewPermissions() === false) {
            this.pushPath(index());
            return;
        }

        this.fetchEntity(this.props.params);
    }

    componentDidUpdate() {
        const data = this.props.entity.get('data');
        const parents = this.props.entity.get('parents');

        if (data === null || this.crumbsDefined) {
            return;
        }

        this.crumbsDefined = true;
        const crumb = getCrumbs(this.props.translation.breadcrumbs);
        this.setBreadcrumbs(this.getBreadcrumbs(crumb, data, parents));
    }

    shouldComponentUpdate(nextProps) {
        return nextProps.entity !== this.props.entity;
    }

    render() {
        const { entity } = this.props;

        if (entity.get('fetching') || entity.get('data') === null) {
            return null;
        }

        return (
            <div className={css.container}>
                {this.renderForm()}
            </div>
        );
    }

    checkEditPermissions() {}
    checkViewPermissions() {}

    renderForm() {
        return <div>
            <Card>
                {this.renderMainForm()}
                {this.renderActions()}
            </Card>
            {this.renderAfterForm()}
        </div>;
    }

    renderActions() {
        const {
            lastUpdate
        } = this.props.translation.entityView;

        const updateData = this.props.entity.getIn(['data', 'lastUpdate']);

        return <FormActionGroup>
            {this.checkEditPermissions() === false
                ? <div style={{ marginLeft: -8 }}/>
                : this.renderButtons()
            }
            {lastUpdate && <div className={css.lastUpdate}>
                <Text type="body-3">
                    {lastUpdate(
                        updateData.getIn(['user', 'firstName']),
                        updateData.getIn(['user', 'secondName']),
                        dateFormatter(this.props.locale, updateData.get('date'), 'dateTime')
                    )}
                </Text>
            </div>}
        </FormActionGroup>;
    }

    renderButtons() {
        return <FlatButton
            onTouchTap={() => this.handleEdit()}
            label={this.props.translation.entityView.editButton}
            icon={<ModeEdit />}
        />;
    }

    renderAfterForm() {}

    pushPath(path) {
        this.context.router.push(path);
    }

    setBreadcrumbs(crumbs) {
        this.props.dispatch(setBreadcrumbs(crumbs));
    }
}
