import * as Nav from 'nav';

function createCrumb(res, resName, navName) {
    return function(navParams, title) {
        const nav = Nav[navName || resName];
        return {
            title: title || res[resName],
            url: nav && nav.apply(null, navParams)
        };
    };
}

export function getCrumbs(res) {
    return {
        home: createCrumb(res, 'home', 'adminIndex'),
        signatories: createCrumb(res, 'signatories', 'adminSignatories'),
        newSignatory: createCrumb(res, 'newSignatory', 'adminNewSignatory'),
        signatory: createCrumb(res, 'signatory', 'adminSignatory'),
        templates: createCrumb(res, 'templates', 'adminTemplates'),
        newTemplate: createCrumb(res, 'newTemplate', 'adminNewTemplate'),
        template: createCrumb(res, 'template', 'adminTemplate'),
        dynamicFields: createCrumb(res, 'dynamicFields', 'adminDynamicFields'),
        users: createCrumb(res, 'users', 'adminUsers'),
        newUser: createCrumb(res, 'newUser', 'adminNewUser'),
        user: createCrumb(res, 'user', 'adminUser')
    };
}
