
import React, { Component, PropTypes } from 'react';
import FlatButton from 'material-ui/FlatButton';
import Add from 'material-ui/svg-icons/content/add-circle-outline';
import { FormActionGroup } from 'dumb/form';
import Card from 'dumb/card';
import { setBreadcrumbs } from 'actions/main-layout-action-creators';
import css from './base-entity.css';
import { getCrumbs } from './breadcrumbs-helper';
import { index } from 'nav';

const { object, func } = PropTypes;

export default class BaseEntityList extends Component {
    static contextTypes = {
        router: object
    };

    static propTypes = {
        dispatch: func,
        list: object,
        params: object,
        getEntityList: func,
        translation: object
    };

    componentWillMount() {
        if (this.checkViewPermissions() === false) {
            this.pushPath(index());
            return;
        }

        this.fetchList(this.props.params);
    }

    componentDidUpdate() {
        const data = this.props.list.get('items');
        const parents = this.props.list.get('parents');

        if (data === null || this.crumbsDefined) {
            return;
        }

        this.crumbsDefined = true;
        const crumb = getCrumbs(this.props.translation.breadcrumbs);
        this.setBreadcrumbs(this.getBreadcrumbs(crumb, data, parents));
    }

    shouldComponentUpdate(nextProps) {
        return nextProps.list !== this.props.list;
    }

    render() {
        const { list } = this.props;

        if (list.get('fetching') || list.get('items') === null) {
            return null;
        }

        return (
            <div className={css.container}>
                <Card>
                    {this.renderMainForm()}
                    {this.renderActions()}
                </Card>
                {this.renderAfterForm()}
            </div>
        );
    }

    checkAddPermissions() {}
    checkViewPermissions() {}

    renderActions() {
        if (this.checkAddPermissions() === false) {
            return null;
        }

        const buttonName = this.getAddButtonName();

        if (buttonName) {
            return <FormActionGroup>
                <FlatButton
                    icon={<Add />}
                    label={buttonName}
                    onTouchTap={() => this.handleAdd()}
                />
            </FormActionGroup>
        }
    }

    renderAfterForm() {}

    pushPath(path) {
        this.context.router.push(path);
    }

    setBreadcrumbs(crumbs) {
        this.props.dispatch(setBreadcrumbs(crumbs));
    }
}
