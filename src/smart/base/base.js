import React, { Component, PropTypes } from 'react';
import { setBreadcrumbs } from 'actions/main-layout-action-creators';
import { getCrumbs } from './breadcrumbs-helper';

const { object, func, string } = PropTypes;

export default class Base extends Component {
    static contextTypes = {
        router: object.isRequired
    };

    componentWillMount() {
        const crumb = getCrumbs(this.props.translation.breadcrumbs);
        this.setBreadcrumbs(this.getBreadcrumbs(crumb));
        this.fetchData();
    }

    fetchData() {}

    setBreadcrumbs(crumbs) {
        this.props.dispatch(setBreadcrumbs(crumbs));
    }

    pushPath(path) {
        this.context.router.push(path);
    }
}
