import React, { Component, PropTypes } from 'react';
import { Table, TableCell, TableHeaderCell, TableHeaderRow, TableRow } from 'dumb/table';
import Card from 'dumb/card';
import { TextField } from 'dumb/text-field';
import { connect } from 'react-redux';
import translate from 'i18n/translate';
import Base from 'smart/base/base';
import { Content } from 'dumb/layout';
import IconButton from 'material-ui/IconButton';
import DoneIcon from 'material-ui/svg-icons/action/done';
import DeleteIcon from 'material-ui/svg-icons/action/delete-forever';
import {
    getDynamicFieldList,
    resetDynamicFieldList,
    deleteDynamicField,
    saveDynamicField,
    persistDynamicField
} from 'actions/admin-dynamic-field-action-creators';
import css from './dynamic-field-list.css';

const stateToProps = state => ({
    list: state.get('dynamicFieldList')
});

const actionsToProps = dispatch => ({
    getList: () => dispatch(getDynamicFieldList()),
    resetList: () => dispatch(resetDynamicFieldList()),
    deleteItem: (index, id) => dispatch(deleteDynamicField(index, id)),
    saveItem: (index, data) => dispatch(saveDynamicField(index, data)),
    persistDynamicField: (index, data) => dispatch(persistDynamicField(index, data)),
    dispatch
});

@connect(stateToProps, actionsToProps)
@translate('breadcrumbs', 'adminFieldList')
export default class DynamicFields extends Base {
    static contextTypes = {
        router: PropTypes.object.isRequired
    };

    getBreadcrumbs(crumb) {
        return [
            crumb.home(),
            crumb.dynamicFields()
        ];
    }

    fetchData() {
        this.props.getList();
    }

    componentWillUnmount() {
        this.props.resetList();
    }

    shouldComponentUpdate(nextProps) {
        return nextProps.list !== this.props.list;
    }

    render() {
        const items = this.props.list.get('items');

        if (items === null || items.size === 0) {
            return null;
        }

        const {
            key,
            englishName,
            frenchName
        } = this.props.translation.adminFieldList;

        return <Content offset={1}>
            <div style={{width: 800}}>
                <Card>
                    <Table>
                        <thead>
                            <TableHeaderRow>
                                <TableHeaderCell>{key}</TableHeaderCell>
                                <TableHeaderCell>{englishName}</TableHeaderCell>
                                <TableHeaderCell>{frenchName}</TableHeaderCell>
                            </TableHeaderRow>
                        </thead>
                        <tbody>
                            {this.renderRows(items)}
                        </tbody>
                    </Table>
                </Card>
            </div>
        </Content>;
    }

    renderRows(items) {
        const {
            key,
            englishName,
            frenchName,
            saveButton,
            deleteButton
        } = this.props.translation.adminFieldList;

        return items.map((item, i) => <TableRow key={i}>
            <TableCell width={80}>
                <TextField
                    value={item.get('key')}
                    placeholder={key}
                    onChange={key => this.props.persistDynamicField(i, { key })}
                />
            </TableCell>
            <TableCell>
                <TextField
                    value={item.get('englishName')}
                    placeholder={englishName}
                    onChange={englishName => this.props.persistDynamicField(i, { englishName })}
                />
            </TableCell>
            <TableCell>
                <TextField
                    value={item.get('frenchName')}
                    placeholder={frenchName}
                    onChange={frenchName => this.props.persistDynamicField(i, { frenchName })}
                />
            </TableCell>
            <TableCell width="1%">
                <div className={css.controls}>
                    <IconButton
                        tooltip={saveButton}
                        disabled={item.get('sending')}
                        onTouchTap={() => this.props.saveItem(i, item.toJS())}
                    >
                        <DoneIcon />
                    </IconButton>
                    <IconButton
                        tooltip={deleteButton}
                        disabled={item.get('sending')}
                        onTouchTap={() => this.props.deleteItem(i, item.get('id'))}
                    >
                        <DeleteIcon/>
                    </IconButton>
                </div>
            </TableCell>
        </TableRow>);
    }
}
