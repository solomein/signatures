import React, { PropTypes } from 'react';
import SignatoryEdit from 'dumb/signatory-edit';
import { connect } from 'react-redux';
import {
    getSignatory,
    resetSignatory,
    updateSignatory,
    createSignatory,
    deleteSignatory,
    getSignatoryTemplate
} from 'actions/admin-signatory-action-creators';
import BaseEdit from 'smart/base/base-entity-edit';
import translate from 'i18n/translate';
import { Content } from 'dumb/layout';
import { adminSignatories } from 'nav';

const stateToProps = state => ({
    entity: state.get('signatory')
});

const actionsToProps = dispatch => ({
    getEntity: (signatoryId) => dispatch(getSignatory(signatoryId)),
    resetEntity: () => dispatch(resetSignatory()),
    updateEntity: data => dispatch(updateSignatory(data)),
    createEntity: data => dispatch(createSignatory(data)),
    deleteEntity: data => dispatch(deleteSignatory(data)),
    getSignatoryTemplate: (id, signatoryId) => dispatch(getSignatoryTemplate(id, signatoryId)),
    dispatch
});

@connect(stateToProps, actionsToProps)
@translate('breadcrumbs', 'adminSignatoryOperationMessages')
export default class Signatory extends BaseEdit {
    static contextTypes = {
        router: PropTypes.object.isRequired
    };

    behavior = {
        delete: {
            redirect: () => adminSignatories()
        },
        update: {
            redirect: () => adminSignatories()
        }
    };

    operationMessages = () => this.props.translation.adminSignatoryOperationMessages;

    fetchEntity(p) {
        this.props.getEntity(p.signatoryId);
    }

    componentWillUnmount() {
        this.props.resetEntity();
    }

    getBreadcrumbs(crumb) {
        return [
            crumb.home(),
            crumb.signatories(),
            crumb.signatory()
        ];
    }

    isNew() {
        return !this.props.params.signatoryId;
    }

    renderMainForm() {
        return <Content offset={1}>
            <div style={{width: 540}}>
                <SignatoryEdit
                    data={this.props.entity.get('data')}
                    templates={this.props.entity.getIn(['associated', 'templates'])}
                    onSave={data => this.handleSave(data)}
                    onDelete={data => this.handleDelete(data)}
                    onTemplateChange={templateId => this.handleTemplateChange(templateId)}
                    hideFields={this.props.entity.get('templateFetching')}
                />
            </div>
        </Content>;
    }

    handleSave(data) {
        if (this.isNew()) {
            this.props.createEntity(data.toJS());
        }
        else {
            this.props.updateEntity(data.toJS());
        }
    }

    handleDelete(data) {
        this.props.deleteEntity(data);
    }

    handleTemplateChange(templateId) {
        this.props.getSignatoryTemplate(
            templateId,
            this.props.entity.getIn(['data', 'id'])
        );
    }
}
