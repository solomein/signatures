import React, { PropTypes } from 'react';
import { adminUsers } from 'nav';
import { UserEdit, UserCreate } from 'dumb/user';
import { connect } from 'react-redux';
import {
    getAdmin,
    updateAdmin,
    createAdmin,
    deleteAdmin,
    resetAdmin
} from 'actions/admin-admin-action-creators';
import {
    getAndStoreAdminProfile
} from 'actions/admin-action-creators';
import BaseEdit from 'smart/base/base-entity-edit';
import translate from 'i18n/translate';
import { Content } from 'dumb/layout';

const stateToProps = state => ({
    entity: state.get('baseEntity'),
    adminId: state.getIn(['admin', 'id']),
    admin: state.get('admin')
});

const actionsToProps = dispatch => ({
    getEntity: adminId => dispatch(getAdmin(adminId)),
    resetEntity: () => dispatch(resetAdmin()),
    updateEntity: data => dispatch(updateAdmin(data)),
    createEntity: data => dispatch(createAdmin(data)),
    deleteEntity: data => dispatch(deleteAdmin(data)),
    getAdminProfile: () => dispatch(getAndStoreAdminProfile()),
    dispatch
});

@connect(stateToProps, actionsToProps)
@translate('breadcrumbs', 'userOperationMessages')
export default class SmartUserEdit extends BaseEdit {
    static contextTypes = {
        router: PropTypes.object.isRequired
    };

    behavior = {
        create: {
            redirect: () => adminUsers()
        },
        delete: {
            redirect: () => adminUsers()
        },
        update: {
            redirect: () => adminUsers(),
            onSuccess: () => {
                if (this.isSelf()) {
                    this.props.getAdminProfile();
                }
            }
        }
    };

    operationMessages = () => this.props.translation.userOperationMessages;

    fetchEntity(p) {
        this.props.getEntity(p.userId);
    }

    componentWillUnmount() {
        this.props.resetEntity();
    }

    getBreadcrumbs(crumb) {
        return[
            crumb.home(),
            crumb.users(),
            this.isNew() ? crumb.newUser() : crumb.user()
        ];
    }

    componentWillReceiveProps(nextProps, nextState) {
        const id = this.props.params.userId;
        const nextId = nextProps.params.userId;
        if (id && nextId && id !== nextId) {
            this.props.getEntity(nextId);
            return;
        }

        super.componentWillReceiveProps(nextProps, nextState);
    }

    isNew() {
        return !this.props.params.userId;
    }

    isSelf() {
        return this.props.params.userId === this.props.adminId;
    }

    renderMainForm() {
        const { entity } = this.props;

        const props = {
            data: entity.get('data'),
            onSave: data => this.handleSave(data),
            onDelete: data => this.handleDelete(data),
            canEditRights: !this.isSelf()
        };

        const Component = this.isNew() ? UserCreate : UserEdit;

        return <Content offset={1}>
            <div style={{width: 600}}>
                {React.createElement(Component, props)}
            </div>
        </Content>
    }

    handleSave(data) {
        if (this.isNew()) {
            this.props.createEntity(data.toJS());
        }
        else {
            this.props.updateEntity(data.toJS());
        }
    }

    handleDelete(data) {
        this.props.deleteEntity(data);
    }
}
