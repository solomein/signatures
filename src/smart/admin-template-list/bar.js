import React, { Component, PropTypes } from 'react';
import { TextField } from 'dumb/text-field';
import Dropdown, { getTranslation } from 'dumb/dropdown';
import { Bar } from 'dumb/layout';
import AppBar, { AppBarCell, AppBarControlWrap } from 'dumb/app-bar';
import SearchIcon from 'material-ui/svg-icons/action/search';
import { templateLanguage, templateStatus } from 'provider';
import debounce from 'lodash/debounce';
import translate from 'i18n/translate';

const { func, object } = PropTypes;

@translate('templateLanguage', 'templateStatus', 'adminTemplateList')
export default class SearchBar extends Component {
    static propTypes = {
        onChange: func,
        translation: object
    };

    constructor(props) {
        super(props);
        this.state = {
            lang: templateLanguage.getLanguageFilter()[0],
            status: templateStatus.getStatusFilter()[0],
            term: ''
        };

        this.callChange = debounce(data => this.props.onChange(data), 250);
    }

    shouldComponentUpdate(nextProps, nextState) {
        return nextState.lang !== this.state.lang
            || nextState.status !== this.state.status
            || nextState.term !== this.state.term;
    }

    render() {
        const langs = getTranslation(
            templateLanguage.getLanguageFilter(),
            this.props.translation.templateLanguage
        );

        const statuses = getTranslation(
            templateStatus.getStatusFilter(),
            this.props.translation.templateStatus
        );

        return <Bar offset={1}>
            <AppBar second={true}>
                <AppBarCell>
                    <AppBarControlWrap>
                        <SearchIcon />
                    </AppBarControlWrap>
                    <AppBarControlWrap style={{ width: 360, height: 41 }}>
                        <TextField
                            placeholder={this.props.translation.adminTemplateList.search}
                            onChange={val => this.handleTermChange(val)}
                            value={this.state.term}
                        />
                    </AppBarControlWrap>
                    <AppBarControlWrap>
                        <Dropdown
                            value={this.state.lang}
                            data={langs}
                            onChange={val => this.handleLangChange(val)}
                        />
                    </AppBarControlWrap>
                    <AppBarControlWrap>
                        <Dropdown
                            value={this.state.status}
                            data={statuses}
                            onChange={val => this.handleStatusChange(val)}
                        />
                    </AppBarControlWrap>
                </AppBarCell>
            </AppBar>
        </Bar>;
    }

    handleLangChange(lang) {
        const data = Object.assign({}, this.state, { lang });
        this.setState(data);
        this.callChange(data);
    }

    handleStatusChange(status) {
        const data = Object.assign({}, this.state, { status });
        this.setState(data);
        this.callChange(data);
    }

    handleTermChange(term) {
        const data = Object.assign({}, this.state, { term });
        this.setState(data);
        this.callChange(data);
    }
}
