import React, { Component, PropTypes } from 'react';
import Card from 'dumb/card';
import { connect } from 'react-redux';
import translate from 'i18n/translate';
import Base from 'smart/base/base';
import { Content } from 'dumb/layout';
import { List } from 'dumb/list';
import BulbIcon from 'material-ui/svg-icons/action/lightbulb-outline';
import {
    getTemplateList,
    resetTemplateList,
    getTemplateListPart
} from 'actions/admin-template-action-creators';
import { adminTemplate } from 'nav';
import SearchBar from './bar';
import ScrollContainer from 'dumb/scroll-container';
import { getTemplateName } from 'i18n/helpers';

const stateToProps = state => ({
    list: state.get('baseList')
});

const actionsToProps = dispatch => ({
    getTemplateList: (term, lang, status) => dispatch(getTemplateList(term, lang, status)),
    getTemplateListPart: (term, lang, status) => dispatch(getTemplateListPart(term, lang, status)),
    resetTemplateList: () => dispatch(resetTemplateList()),
    dispatch
});

@connect(stateToProps, actionsToProps)
@translate('breadcrumbs', 'templateLanguage')
export default class TemplateList extends Base {
    static contextTypes = {
        router: PropTypes.object.isRequired
    };

    getBreadcrumbs(crumb) {
        return [
            crumb.home(),
            crumb.templates()
        ];
    }

    fetchData() {
        this.props.getTemplateListPart();
    }

    componentWillUnmount() {
        this.props.resetTemplateList();
    }

    shouldComponentUpdate(nextProps) {
        return nextProps.list !== this.props.list;
    }

    render() {
        return <ScrollContainer onBounce={() => this.handleBounce()}>
            {this.renderBar()}
            <Content offset={2}>
                {this.renderList()}
            </Content>
        </ScrollContainer>;
    }

    renderList() {
        const { list } = this.props;

        if (list.get('items') === null) {
            return null;
        }

        return <div style={{ width: 540 }}>
            <Card>
                <List
                    onSelect={data => this.pushPath(adminTemplate(data.id))}
                    data={list.get('items')}
                    dataConverter={item => {
                        const iconColor = item.get('isActive') ? '#4CAF50' : '#BDBDBD';
                        const opacity = item.get('isActive') ? 1 : 0.45;
                        const lang = this.props.translation.templateLanguage[item.get('lang')];
                        const controls = [
                            <div style={{marginRight: 8}}><BulbIcon color={iconColor}/></div>
                        ];
                        const name = getTemplateName(this.props.locale, item);

                        return {
                            id: item.get('id'),
                            primaryText: <span style={{ opacity }}>{name}</span>,
                            secondaryText: <span style={{ opacity }}>{lang}</span>,
                            controls
                        };
                    }}
                />
            </Card>
        </div>
    }

    renderBar() {
        return <SearchBar
            onChange={data => this.handleSearch(data)}
        />;
    }

    handleBounce() {
        if (this.props.list.get('isCompleted')) {
            return;
        }
        this.props.getTemplateListPart();
    }

    handleSearch(data) {
        this.props.getTemplateList(data.term, data.lang, data.status);
    }
}
