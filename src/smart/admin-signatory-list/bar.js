import React, { Component, PropTypes } from 'react';
import { TextField } from 'dumb/text-field';
import { Bar } from 'dumb/layout';
import AppBar, { AppBarCell, AppBarControlWrap } from 'dumb/app-bar';
import SearchIcon from 'material-ui/svg-icons/action/search';
import debounce from 'lodash/debounce';
import translate from 'i18n/translate';

const { func, object } = PropTypes;

@translate('adminSignatoryList')
export default class SearchBar extends Component {
    static propTypes = {
        onChange: func,
        translation: object
    };

    constructor(props) {
        super(props);
        this.state = {
            term: ''
        };

        this.callChange = debounce(data => this.props.onChange(data), 250);
    }

    shouldComponentUpdate(nextProps, nextState) {
        return nextState.term !== this.state.term;
    }

    render() {
        return <Bar offset={1}>
            <AppBar second={true}>
                <AppBarCell>
                    <AppBarControlWrap>
                        <SearchIcon />
                    </AppBarControlWrap>
                    <AppBarControlWrap style={{ width: 640, height: 41 }}>
                        <TextField
                            placeholder={this.props.translation.adminSignatoryList.search}
                            onChange={val => this.handleTermChange(val)}
                            value={this.state.term}
                        />
                    </AppBarControlWrap>
                </AppBarCell>
            </AppBar>
        </Bar>;
    }

    handleTermChange(term) {
        const data = Object.assign({}, this.state, { term });
        this.setState(data);
        this.callChange(data);
    }
}
