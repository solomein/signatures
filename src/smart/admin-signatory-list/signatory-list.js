import React, { PropTypes } from 'react';
import { Table, TableCell, TableHeaderCell, TableHeaderRow, TableRow } from 'dumb/table';
import Card from 'dumb/card';
import { connect } from 'react-redux';
import translate from 'i18n/translate';
import Base from 'smart/base/base';
import { Content } from 'dumb/layout';
import { adminSignatory } from 'nav';
import {
    getSignatoryList,
    resetSignatoryList,
    getSignatoryListPart
} from 'actions/admin-signatory-action-creators';
import ScrollContainer from 'dumb/scroll-container';
import SearchBar from './bar';
import { getTemplateName } from 'i18n/helpers';
import dateFormatter from 'i18n/date-formatter';

const stateToProps = state => ({
    list: state.get('baseList')
});

const actionsToProps = dispatch => ({
    getSignatoryList: (term) => dispatch(getSignatoryList(term)),
    getSignatoryListPart: (term) => dispatch(getSignatoryListPart(term)),
    resetSignatoryList: () => dispatch(resetSignatoryList()),
    dispatch
});

@connect(stateToProps, actionsToProps)
@translate('breadcrumbs', 'templateLanguage', 'adminSignatoryList')
export default class Signatories extends Base {
    static contextTypes = {
        router: PropTypes.object.isRequired
    };

    getBreadcrumbs(crumb) {
        return [
            crumb.home(),
            crumb.signatories()
        ];
    }

    fetchData() {
        this.props.getSignatoryListPart();
    }

    componentWillUnmount() {
        this.props.resetSignatoryList();
    }

    shouldComponentUpdate(nextProps) {
        return nextProps.list !== this.props.list;
    }

    render() {
        return <ScrollContainer onBounce={() => this.handleBounce()}>
            {this.renderBar()}
            <Content offset={2}>
                {this.renderList()}
            </Content>
        </ScrollContainer>;
    }

    renderList() {
        const { list } = this.props;

        if (list.get('items') === null) {
            return null;
        }

        const {
            email,
            templateName,
            language,
            lastConnect
        } = this.props.translation.adminSignatoryList;

        return <div style={{ width: 800 }}>
            <Card>
                <Table divided selectable>
                    <thead>
                        <TableHeaderRow>
                            <TableHeaderCell>{email}</TableHeaderCell>
                            <TableHeaderCell>{templateName}</TableHeaderCell>
                            <TableHeaderCell>{language}</TableHeaderCell>
                            <TableHeaderCell>{lastConnect}</TableHeaderCell>
                        </TableHeaderRow>
                    </thead>
                    <tbody>
                        {list.get('items').map(item => this.renderListItem(item))}
                    </tbody>
                </Table>
            </Card>
        </div>;
    }

    renderListItem(item) {
        const date = dateFormatter(
            this.props.locale,
            item.get('lastConnect'),
            'dateTime'
        );
        const name = getTemplateName(
            this.props.locale,
            item.get('template')
        );
        const lang = this.props.translation
            .templateLanguage[item.getIn(['template', 'lang'])];
        const id = item.get('id');
        return <TableRow
            onTouchTap={() => this.pushPath(adminSignatory(id))}
            key={id}
        >
            <TableCell>{item.get('email')}</TableCell>
            <TableCell>{name}</TableCell>
            <TableCell>{lang}</TableCell>
            <TableCell width="1%">{date}</TableCell>
        </TableRow>;
    }

    renderBar() {
        return <SearchBar
            onChange={data => this.handleSearch(data)}
        />;
    }

    handleBounce() {
        if (this.props.list.get('isCompleted')) {
            return;
        }
        this.props.getSignatoryListPart();
    }

    handleSearch(data) {
        this.props.getSignatoryList(data.term);
    }
}
