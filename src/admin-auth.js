import Cookie from 'js-cookie';
import { admin } from 'provider';
import config from 'config';

const SID_KEY = 'a_id';
const LOCALE_KEY = 'a_locale';

export function loggedIn() {
    if (process.env.APP_SKIP_AUTH) {
        return true;
    }

    return Boolean(Cookie.get(SID_KEY));
}

export function login(loginName, password, locale) {
    Cookie.set(LOCALE_KEY, locale);
    return admin.login(loginName, password).then(() => {
        if (config.appFakes) {
            Cookie.set(SID_KEY, Math.random());
        }
    });
}

export function logout() {
    return admin.logout().then(() => {
        Cookie.remove(SID_KEY);
    });
}

export function getLastLocale() {
    return Cookie.get(LOCALE_KEY) || 'en-GB';
}

export function clear() {
    Cookie.remove(SID_KEY);
}
