import React from 'react';
import ReactDOM from 'react-dom';
import injectTapEvent from 'react-tap-event-plugin';
import { Provider } from 'react-redux';
import getStore from 'utils/configure-store';
import { setBaseUrl } from 'utils/ajax';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import themeSettings from 'theme';
import TranslationProvider from 'i18n/translation-provider';
import config from 'config';
import 'styles/index.css';
import Routing from './routing';

setBaseUrl(config.appApi);
injectTapEvent();

const store = getStore();
const theme = getMuiTheme(themeSettings);

window.Promise = Promise;

ReactDOM.render((
    <Provider store={store}>
        <MuiThemeProvider muiTheme={theme}>
            <TranslationProvider>
                {Routing}
            </TranslationProvider>
        </MuiThemeProvider>
    </Provider>
), document.querySelector('main'));
