import * as Types from './action-types';
import {
    adminTemplateMultipart,
    adminTemplate,
    adminDynamicField,
    templateLanguage
} from 'provider';
import {
    getMultipartList,
    getMultipartListPart,
    resetBaseEntity,
    sendBaseEntity
} from './helpers';

export const getTemplateList = getMultipartList(
    [Types.GET_BASE_LIST_SUCCESS],
    adminTemplateMultipart,
    adminTemplate.getTemplateList
);

export const getTemplateListPart = getMultipartListPart(
    [Types.GET_BASE_LIST_PART_SUCCESS],
    adminTemplateMultipart,
    adminTemplate.getTemplateList
);

export function resetTemplateList() {
    adminTemplateMultipart.reset();
    return {
        type: Types.RESET_BASE_LIST
    };
}

export function getTemplate(templateId) {
    return dispatch => {
        dispatch({
            type: Types.GET_BASE_ENTITY_REQUEST
        });
        Promise.all([
            adminTemplate.getTemplate(templateId),
            adminDynamicField.getDynamicFieldList(),
            adminTemplate.getAllActiveTemplates()
        ])
        .then(values => dispatch({
            type: Types.GET_BASE_ENTITY_SUCCESS,
            payload: {
                entity: values[0],
                associated: {
                    fields: values[1],
                    languages: templateLanguage.getLanguages(),
                    templates: [
                        { englishName: 'No', frenchName: 'Non' }
                    ].concat(values[2].filter(x => x.id !== templateId))
                }
            }
        }));
    };
}

export const resetTemplate = resetBaseEntity();
export const updateTemplate = sendBaseEntity(adminTemplate.updateTemplate, { type: 'update' });
export const createTemplate = sendBaseEntity(adminTemplate.createTemplate, { type: 'create' });
export const deleteTemplate = sendBaseEntity(adminTemplate.deleteTemplate, { type: 'delete' });
