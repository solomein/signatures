import * as Types from './action-types';
import { adminDynamicField } from 'provider';

export function getDynamicFieldList() {
    return dispatch => {
        adminDynamicField.getDynamicFieldList().then(data => dispatch({
            type: Types.GET_DYNAMIC_FIELD_LIST_SUCCESS,
            payload: {
                data
            }
        }));
    }
}

export function resetDynamicFieldList () {
    return {
        type: Types.RESET_DYNAMIC_FIELD_LIST
    };
}

export function addDynamicField() {
    return {
        type: Types.ADD_DYNAMIC_FIELD
    };
}

export function persistDynamicField(index, data) {
    return {
        type: Types.PERSIST_DYNAMIC_FIELD,
        payload: { index, data }
    };
}

export function deleteDynamicField(index, id) {
    if (id) {
        return dispatch => {
            dispatch({
                type: Types.DELETE_DYNAMIC_FIELD_REQUEST,
                payload: { index }
            });
            adminDynamicField.deleteDynamicField(id)
                .then(() => dispatch({
                    type: Types.DELETE_DYNAMIC_FIELD_SUCCESS,
                    payload: { id }
                }));
        }
    }

    return {
        type: Types.DELETE_DYNAMIC_FIELD_SUCCESS,
        payload: { index }
    };
}

export function saveDynamicField(index, data) {
    return dispatch => {
        dispatch({
            type: Types.SAVE_DYNAMIC_FIELD_REQUEST,
            payload: { index }
        });
        const req = data.id
            ? adminDynamicField.updateDynamicField
            : adminDynamicField.createDynamicField;
        req(data).then(resp => dispatch({
            type: Types.SAVE_DYNAMIC_FIELD_SUCCESS,
            payload: { data: resp, index, cid: data._cid }
        }));
    }
}
