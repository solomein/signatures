import * as Types from './action-types';
import {
    admin
} from 'provider';
import {
    resetBaseEntity,
    resetBaseList,
    sendBaseEntity
} from './helpers';

export function getAdminList() {
    return dispatch => {
        admin.getAdminList().then(data => dispatch({
            type: Types.GET_BASE_LIST_SUCCESS,
            payload: {
                data
            }
        }));
    }
}

export const resetAdminList = resetBaseList();

export function getAdmin(adminId) {
    return dispatch => {
        dispatch({
            type: Types.GET_BASE_ENTITY_REQUEST
        });
        admin.getAdmin(adminId).then(data => dispatch({
            type: Types.GET_BASE_ENTITY_SUCCESS,
            payload: {
                entity: data
            }
        }));
    };
}

export const resetAdmin = resetBaseEntity();
export const updateAdmin = sendBaseEntity(admin.updateAdmin, { type: 'update' });
export const createAdmin = sendBaseEntity(admin.createAdmin, { type: 'create' });
export const deleteAdmin = sendBaseEntity(admin.deleteAdmin, { type: 'delete' });
