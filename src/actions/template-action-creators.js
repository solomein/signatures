import * as Types from './action-types';
import { templateLanguage, templateMultipart, template } from 'provider';
import { getMultipartList, getMultipartListPart } from './helpers';

export function getTemplate(templateId) {
    return dispatch => {
        template.getTemplate(templateId)
            .then(data => dispatch({
                type: Types.GET_TEMPLATE_SUCCESS,
                payload: {
                    entity: data,
                    associated: {
                        languages: templateLanguage.getLanguages()
                    }
                }
            }));
    };
}

export function resetTemplate() {
    return {
        type: Types.RESET_TEMPLATE
    };
}

export const getTemplateList = getMultipartList(
    [Types.GET_BASE_LIST_SUCCESS],
    templateMultipart,
    template.getTemplateList
);

export const getTemplateListPart = getMultipartListPart(
    [Types.GET_BASE_LIST_PART_SUCCESS],
    templateMultipart,
    template.getTemplateList
);

export function resetTemplateList() {
    templateMultipart.reset();
    return {
        type: Types.RESET_BASE_LIST
    };
}

export function setupTemplate(data) {
    return dispatch => {
        dispatch({ type: Types.SETUP_TEMPLATE_REQUEST });
        template.setupTemplate(data)
            .then(() => dispatch({
                type: Types.SETUP_TEMPLATE_SUCCESS
            }))
            .catch(() => dispatch({
                type: Types.SETUP_TEMPLATE_FAILURE
            }));
    };
}
