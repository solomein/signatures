import * as Types from './action-types';
import {
    adminSignatoryMultipart,
    adminSignatory,
    adminTemplate
} from 'provider';
import {
    getMultipartList,
    getMultipartListPart,
    resetBaseEntity,
    sendBaseEntity
} from './helpers';

export const getSignatoryList = getMultipartList(
    [Types.GET_BASE_LIST_SUCCESS],
    adminSignatoryMultipart,
    adminSignatory.getSignatoryList
);

export const getSignatoryListPart = getMultipartListPart(
    [Types.GET_BASE_LIST_PART_SUCCESS],
    adminSignatoryMultipart,
    adminSignatory.getSignatoryList
);

export function resetSignatoryList() {
    adminSignatoryMultipart.reset();
    return {
        type: Types.RESET_BASE_LIST
    };
}

export function getSignatory(signatoryId) {
    return dispatch => {
        dispatch({
            type: Types.GET_SIGNATORY_REQUEST
        });
        Promise.all([
            adminSignatory.getSignatory(signatoryId),
            adminTemplate.getAllActiveTemplates()
        ])
        .then(values => dispatch({
            type: Types.GET_SIGNATORY_SUCCESS,
            payload: {
                entity: values[0],
                associated: {
                    templates: [
                        { englishName: 'No', frenchName: 'Non' }
                    ].concat(values[1])
                }
            }
        }));
    };
}

export function getSignatoryTemplate(templateId, signatoryId) {
    return dispatch => {
        dispatch({
            type: Types.GET_SIGNATORY_TEMPLATE_REQUEST
        });
        adminSignatory.getSignatoryTemplate(templateId, signatoryId)
            .then(data => dispatch({
                type: Types.GET_SIGNATORY_TEMPLATE_SUCCESS,
                payload: {
                    entity: data
                }
            }));
    };
}

export const resetSignatory = resetBaseEntity(Types.RESET_SIGNATORY);
export const updateSignatory = sendBaseEntity(
    adminSignatory.updateSignatory,
    { type: 'update' },
    [Types.SEND_SIGNATORY_REQUEST, Types.SEND_SIGNATORY_SUCCESS]
);
export const createSignatory = sendBaseEntity(
    adminSignatory.createSignatory,
    { type: 'create' },
    [Types.SEND_SIGNATORY_REQUEST, Types.SEND_SIGNATORY_SUCCESS]
);
export const deleteSignatory = sendBaseEntity(
    adminSignatory.deleteSignatory,
    { type: 'delete' },
    [Types.SEND_SIGNATORY_REQUEST, Types.SEND_SIGNATORY_SUCCESS]
);
