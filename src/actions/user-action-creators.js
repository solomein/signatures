import * as Types from './action-types';
import { template, user } from 'provider';

export function getCurrentTemplate(templateId) {
    return dispatch => {
        template.getTemplate(templateId)
            .then(data => dispatch({
                type: Types.GET_USER_CURRENT_TEMPLATE_SUCCESS,
                payload: data
            }));
    };
}

export function setUserLocale(locale) {
    return {
        type: Types.SET_USER_LOCALE,
        payload: locale
    };
}

export function getUserProfile() {
    return {
        type: Types.GET_USER_PROFILE_SUCCESS,
        payload: user.getStoredProfile()
    };
}

export function getAndStoreUserProfile() {
    return dispatch => {
        user.storeProfile().then(data => dispatch({
            type: Types.GET_USER_PROFILE_SUCCESS,
            payload: data
        }));
    };
}
