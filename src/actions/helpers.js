import * as Types from './action-types';

export function getCreator(type) {
    return function creator(payload) {
        return { type, payload };
    }
}

export function getMultipartList(types, multipart, req) {
    return function (...args) {
        return dispatch => {
            const params = multipart
                .reset()
                .getNextPartParams.apply(multipart, args);
            req(params).then(data => dispatch({
                type: types[0],
                payload: {
                    data,
                    expectedCount: multipart.expectedCount
                }
            }));
        }
    }
}

export function getMultipartListPart(types, multipart, req) {
    return function (...args) {
        return dispatch => {
            const params = multipart
                .getNextPartParams.apply(multipart, args);
            req(params).then(data => dispatch({
                type: types[0],
                payload: {
                    data,
                    expectedCount: multipart.expectedCount
                }
            }));
        }
    }
}

export function resetBaseList() {
    return function () {
        return {
            type: Types.RESET_BASE_LIST
        };
    };
}

export function resetBaseEntity(actionType) {
    return function () {
        return {
            type: actionType || Types.RESET_BASE_ENTITY
        };
    };
}

export function sendBaseEntity(reqSource, { type }, actionTypes = [])  {
    return function sendEntity(...args) {
        return dispatch => {
            dispatch({
                type: actionTypes[0] || Types.SEND_BASE_ENTITY_REQUEST,
                payload: type
            });
            reqSource.apply(null, args)
                .then(() => dispatch({
                    type: actionTypes[1] || Types.SEND_BASE_ENTITY_SUCCESS
                }));
        }
    }
}
