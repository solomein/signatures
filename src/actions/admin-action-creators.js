import * as Types from './action-types';
import { admin } from 'provider';

export function setAdminLocale(locale) {
    return {
        type: Types.SET_ADMIN_LOCALE,
        payload: locale
    };
}

export function getAdminProfile() {
    return {
        type: Types.GET_ADMIN_PROFILE_SUCCESS,
        payload: admin.getStoredProfile()
    };
}

export function getAndStoreAdminProfile() {
    return dispatch => {
        admin.storeProfile().then(data => dispatch({
            type: Types.GET_ADMIN_PROFILE_SUCCESS,
            payload: data
        }));
    };
}
