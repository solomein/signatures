import * as Types from './action-types';

export function setBreadcrumbs(payload) {
    return {
        type: Types.SET_BREADCRUMBS,
        payload
    };
}

export function showMessage(message) {
    return dispatch => {
        setTimeout(() => {
            dispatch({
                type: Types.SHOW_MESSAGE,
                message: {
                    text: message,
                    stamp: Date.now()
                }
            })
        }, 0);
    };
}
export function removeMessage(stamp) {
    return {
        type: Types.REMOVE_MESSAGE,
        stamp
    };
}
