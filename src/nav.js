export function index() {
    return '/';
}

export function enter() {
    return '/enter';
}

export function templates() {
    return '/templates';
}

export function template(templateId) {
    return `${templates()}/${templateId}`;
}

export function adminIndex() {
    return '/admin';
}

export function adminLogin() {
    return `${adminIndex()}/login`;
}

export function adminSignatories() {
    return `${adminIndex()}/signatories`;
}

export function adminSignatory(signatoryId) {
    return `${adminSignatories()}/${signatoryId}`;
}

export function adminTemplates() {
    return `${adminIndex()}/templates`;
}

export function adminNewTemplate() {
    return `${adminTemplates()}/new`;
}

export function adminTemplate(templateId) {
    return `${adminTemplates()}/${templateId}`;
}

export function adminFields() {
    return `${adminIndex()}/fields`;
}

export function adminUsers() {
    return `${adminIndex()}/users`;
}

export function adminNewUser() {
    return `${adminUsers()}/new`;
}

export function adminUser(userId) {
    return `${adminUsers()}/${userId}`;
}

export function adminResetPassword() {
    return `${adminIndex()}/resetPassword`;
}
