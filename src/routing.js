import React from 'react';
import { Router, Route, IndexRoute, hashHistory } from 'react-router';
import { connected, clear as clearUserConnect } from 'user-connect';
import { loggedIn, clear as clearAdminAuth} from 'admin-auth';
import { user, admin } from 'provider';
import { addHttpStatusHandler } from 'utils/ajax';
import { MainLayoutAdmin, MainLayoutSignatory } from 'smart/main-layout';
import Login from 'smart/login';
import AdminHome from 'smart/admin-home';
import AdminTemplateList from 'smart/admin-template-list';
import AdminTemplate from 'smart/admin-template';
import AdminDynamicFieldList from 'smart/admin-dynamic-field-list';
import AdminUserList from 'smart/admin-user-list';
import AdminUser from 'smart/admin-user';
import AdminSignatoryList from 'smart/admin-signatory-list';
import AdminSignatory from 'smart/admin-signatory';
import ResetPassword from 'smart/reset-password';
import * as Nav from 'nav';
import TemplateList from 'smart/template-list';
import TemplateConfiguration from 'smart/template-configuration';
import Enter from 'smart/enter';

addHttpStatusHandler(status => {
    if (status === 401) {
        if (/#\/admin/.test(location.hash)) {
            clearAdminAuth();
            hashHistory.replace(Nav.adminLogin());
        }
        else {
            clearUserConnect()
            hashHistory.replace(Nav.enter());
        }
    }
});

function checkForConnection(nextState, replace, callback) {
    if (!connected()) {
        replace({
            pathname: Nav.enter(),
            state: { nextPathname: nextState.location.pathname }
        });
        callback();
    }
    else {
        user.storeProfile().then(() => callback());
    }
}

function checkForAuth(nextState, replace, callback) {
    if (!loggedIn()) {
        replace({
            pathname: Nav.adminLogin(),
            state: { nextPathname: nextState.location.pathname }
        });
        callback();
    }
    else {
        admin.storeProfile().then(() => callback());
    }
}

function checkForUnauth(nextState, replace) {
    if (loggedIn()) {
        replace(Nav.adminIndex());
    }
}

const Routing = (
    <Router history={hashHistory}>
        <Route path={Nav.enter()} component={Enter} />
        <Route component={MainLayoutSignatory} onEnter={checkForConnection}>
            <Route path={Nav.index()} component={TemplateList} />
            <Route path={Nav.template(':templateId')} component={TemplateConfiguration} />
        </Route>
        <Route path={Nav.adminLogin()} component={Login} onEnter={checkForUnauth}/>
        <Route path={Nav.adminIndex()} component={MainLayoutAdmin} onEnter={checkForAuth}>
            <IndexRoute component={AdminHome} />
            <Route path={Nav.adminSignatories()} component={AdminSignatoryList} />
            <Route path={Nav.adminSignatory(':signatoryId')} component={AdminSignatory} />
            <Route path={Nav.adminTemplates()} component={AdminTemplateList} />
            <Route path={Nav.adminNewTemplate()} component={AdminTemplate} />
            <Route path={Nav.adminTemplate(':templateId')} component={AdminTemplate} />
            <Route path={Nav.adminFields()} component={AdminDynamicFieldList} />
            <Route path={Nav.adminUsers()} component={AdminUserList} />
            <Route path={Nav.adminNewUser()} component={AdminUser} />
            <Route path={Nav.adminUser(':userId')} component={AdminUser} />
        </Route>
        <Route path={Nav.adminResetPassword()} component={ResetPassword} />
    </Router>
);

export default Routing;
