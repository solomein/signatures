import Cookie from 'js-cookie';
import { user } from 'provider';
import config from 'config';

const CONNECTION_KEY = 'u_id';
const EMAIL_KEY = 'u_email';
const LOCALE_KEY = 'u_locale';

export function connected() {
    return Boolean(Cookie.get(CONNECTION_KEY));
}

export function connect(email, locale) {
    Cookie.set(LOCALE_KEY, locale);
    return user.connect(email).then(() => {
        localStorage.setItem(EMAIL_KEY, email);
        if (config.appFakes) {
            Cookie.set(CONNECTION_KEY, Math.random());
        }
    });
}

export function getLastEmail() {
    return localStorage.getItem(EMAIL_KEY);
}

export function getLastLocale() {
    return Cookie.get(LOCALE_KEY) || 'en-GB';
}

export function clear() {
    Cookie.remove(CONNECTION_KEY);
}
