import 'whatwg-fetch';
import querystring from 'querystring';
import isFunction from 'lodash/isFunction';
import config from 'config';

let baseUrl = '';
let customHeaders = {};
let httpStatusHandler = () => {};

const DEFAULT_PARAMS = config.cors
    ? {
        credentials: 'include',
        mode: 'cors'
    }
    : {
        credentials: 'same-origin'
    };

function checkStatus(res) {
    if (res.status >= 200 && res.status < 300) {
        return res;
    }

    httpStatusHandler(res.status);

    const error = new Error(res.statusText);
    error.status = res.status;
    throw error;
}

function tryGetJSON(res) {
    if (typeof res.json === 'function' && res.status === 200) {
        return res.json();
    }

    return res;
}

export function setBaseUrl(url) {
    baseUrl = url;
}

export function addHeaders(headers) {
    customHeaders = Object.assign(customHeaders, headers)
}

export function addHttpStatusHandler(handler) {
    httpStatusHandler = handler;
}

function getBaseUrl(url) {
    if (url.indexOf('://') > 0) {
        return url;
    }

    return `${baseUrl}/${url}`;
}

export function getUrl(url, queries = null) {
    const q = querystring.stringify(queries);
    let queryStr = q;

    if (q.length) {
        queryStr = `?${q}`;
    }

    return `${getBaseUrl(url)}${queryStr}`;
}

export function get(url, queries) {
    const params = Object.assign({}, DEFAULT_PARAMS, {
        method: 'get',
        headers: customHeaders
    });

    return window.fetch(getUrl(url, queries), params)
        .then(checkStatus)
        .then(tryGetJSON);
}

function postLike(method) {
    return function post(url, body, headers) {
        const params = Object.assign({}, DEFAULT_PARAMS, {
            method: method,
            headers: Object.assign({}, customHeaders, headers || {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }),
            body: isFunction(body) ? body() : JSON.stringify(body)
        });

        return window.fetch(getBaseUrl(url), params)
            .then(checkStatus)
            .then(tryGetJSON);
    }
}

export const post = postLike('post');
export const put = postLike('put');
export const del = postLike('delete');
