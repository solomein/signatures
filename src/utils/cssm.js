class ClassesWithMods {
    constructor(classes) {
        this.classes = classes;
        this.modMethods = {};
        this.createMethods();
    }

    createMethods() {
        Object.keys(this.classes).forEach(className => {
            if (className.split('_').length === 1) {
                this.modMethods[className] = this.getModMethod(className);
            }
        })
    }

    getModMethod(className) {
        return (mods = {}) => {
            return Object.keys(mods).reduce((result, mod) => {
                const modClass = this.stringifyMod(className, mod, mods[mod]);
                return result += this.getModClassValue(modClass);
            }, this.classes[className]);
        }
    }

    stringifyMod(base, mod, modValue) {
        let result = '';

        if (modValue === false || typeof modValue === 'undefined') {
            return result;
        }

        result += `${base}_${mod}`;

        if (modValue !== true) {
            result += `_${modValue}`
        }

        return result;
    }

    getModClassValue(className) {
        const classValue = this.classes[className];
        return classValue ?  ` ${classValue}` : '';
    }
}

export function withMods(classes) {
    return new ClassesWithMods(classes).modMethods;
}
