if (!('remove' in Element.prototype)) {
    Element.prototype.remove = function() {
        if (this.parentNode) {
            this.parentNode.removeChild(this);
        }
    };
}

export default class FileUploader {
    constructor({ request, onUpload, onSuccess, onFail, onAllComplete,
        inputName='file' }) {
        this._props = {
            request,
            onUpload,
            onSuccess,
            onFail,
            onAllComplete,
            inputName
        };
        this._formContainer = new FormContainer(inputName, this._upload);
        this._reqs = new Map();
        this._canceled = [];
    }

    openDialog = () => {
        this._formContainer.click();
    };

    destroy() {
        this._formContainer.remove();
    }

    cancel(lastModified) {
        if (this._reqs.has(lastModified)) {
            this._canceled.push(lastModified);
            this._reqs.get(lastModified).abort();
        }
    }

    _upload = (file) => {
        const f = {
            id: null,
            name: file.name,
            lastModified: file.lastModified
        };
        const req = this._props.request(this._formContainer.getForm());

        req
            .then(id => {
                f.id = id;
                this._props.onSuccess(f);
                this._clear(f);
            })
            .catch(error => {
                if (this._canceled.indexOf(f.lastModified) < 0) {
                    this._props.onFail(file, error);
                }
                this._clear(f);
            })

        this._reqs.set(f.lastModified, req);
        this._props.onUpload(f);
    };

    _clear = (f) => {
        this._reqs.delete(f.lastModified);
        if (!this._reqs.size) {
            this._formContainer.clearInput();
            this._props.onAllComplete();
        }
    };
}

class FormContainer {
    constructor(inputName, callback) {
        this._props = { callback };
        this._form = this._createForm();
        this._input = this._createInput(inputName);
        this._form.appendChild(this._input);
        document.body.appendChild(this._form);
        this._handleChange = this._handleChange.bind(this);
        this._input.addEventListener('change', this._handleChange, false);
    }

    click() {
        this._input.click();
    }

    remove() {
        this._input.removeEventListener('change', this._handleChange);
        this._form.remove();
    }

    clearInput() {
        this._input.value = null;
    }

    getForm() {
        return this._form;
    }

    _createForm() {
        const form = document.createElement('form');
        form.style.display = 'none';

        return form;
    }

    _createInput(inputName) {
        const input = document.createElement('input');
        input.type = 'file';
        input.name = inputName;

        return input;
    }

    _handleChange() {
        const files = this._input.files;
        let data;
        if (files) {
            data = files[0]
        }
        else {
            data = {
                name: this._input.value.split(/(\\|\/)/g).pop(),
                lastModified: Date.now()
            };
        }
        this._props.callback(data);
    }
}
