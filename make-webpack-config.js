const path = require('path');
const webpack = require('webpack');
const autoprefixer = require('autoprefixer');
const precss = require('precss');
var HtmlWebpackPlugin = require('html-webpack-plugin');

const BUILD_PATH = path.resolve(__dirname, 'build');
const CONTENT_BASE = path.resolve(__dirname, 'src');

function parseEnv(env, defaultValue) {
    const val = process.env[env] || defaultValue;
    if (val === 'true' || val === 'false') {
        return val;
    }
    return `'${val}'`;
}

function getCssLoaderOptions() {
    const options = process.env.NODE_ENV === 'production'
        ? '[hash:base64:4]'
        : '[name]--[local]--[hash:base64:2]';

    return `!css?modules&importLoaders=1&localIdentName=${options}`;
}

function getPlugins() {
    var isProduction = process.env.NODE_ENV === 'production';

    var plugins = [
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': parseEnv('NODE_ENV', 'development'),
            'process.env.APP_FAKES': parseEnv('APP_FAKES', false),
            'process.env.APP_API': parseEnv('APP_API', ''),
            'process.env.APP_SKIP_AUTH': parseEnv('APP_SKIP_AUTH', false)
        }),
        new HtmlWebpackPlugin({
            filename: 'index.html',
            template: path.resolve(CONTENT_BASE, 'index-template.ejs'),
            hash: isProduction,
            title: isProduction ? 'Signatures' : '[DEV] Signatures',
            minify: isProduction ? {
                removeComments: true,
                collapseWhitespace: true
            } : false
        })
    ];

    if (process.env.NODE_ENV === 'production') {
        plugins = plugins.concat([
            new webpack.optimize.DedupePlugin(),
            new webpack.optimize.OccurrenceOrderPlugin(),
            new webpack.optimize.UglifyJsPlugin()
        ])
    }

    return plugins;
}

function getFilesLoaders() {
    const loaders = [`url?limit=1000000&name=[name]-[hash].[ext]&path=${BUILD_PATH}`];

    if (process.env.NODE_ENV === 'production') {
        loaders.push('image-webpack?optimizationLevel=7&interlaced=false');
    }

    return loaders;
}

module.exports = function (params) {
    const baseConfig = {
        entry: [
            path.resolve(CONTENT_BASE, 'main')
        ].concat(params.entry || []),

        module: {
            loaders: [
                {
                    test: /\.js$/,
                    loader: 'babel',
                    exclude: /(node_modules|vendors)/
                },
                {
                    test: /\.css$/,
                    loader: `style?singleton!${getCssLoaderOptions()}!postcss`
                },
                {
                    test: /\.(eot|woff|ttf|svg|png|jpg)$/,
                    loaders: getFilesLoaders()
                }
            ]
        },

        output: {
            path: BUILD_PATH,
            filename: 'bundle.js',
            publicPath: ''
        },

        contentBase: CONTENT_BASE,

        resolve: {
            root: [CONTENT_BASE]
        },

        plugins: getPlugins(),

        postcss() {
            return [autoprefixer, precss];
        }
    }

    return baseConfig;
};
