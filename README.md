1. Install [Node.js](https://nodejs.org/en/)
2. Execute commands in the working directory

```
npm run get-modules
npm run build
```

The bundle will be in the `./build` directory
