const webpack = require('webpack');
const WebpackDevServer = require('webpack-dev-server');
const config = require('./webpack.config.dev');

const server = new WebpackDevServer(webpack(config), {
    publicPath: config.output.publicPath,
    filename: config.output.filename,
    contentBase: config.contentBase,
    stats: {
        colors: true
    }
});

const port = process.env.APP_PORT || '8080';

/*eslint no-console:0*/
server.listen(port, 'localhost', (err) => {
    if (err) {
        console.log(err);
    }
    console.log(`Listening at http://localhost:${port}`);
});
