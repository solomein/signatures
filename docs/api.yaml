swagger: '2.0'
info:
  version: 0.0.1
  title: Signatures
  description: Signatures API
schemes:
  - https
basePath: /api
host: localhost:8080
produces:
  - application/json
paths:
  /connect:
    post:
      description: Connect
      tags:
        - User
      parameters:
        - name: parameters
          in: body
          required: true
          schema:
            $ref: '#/definitions/UserConnectDTO'
      responses:
        204:
          description: Success
        500:
          description: Error
  /getCurrentUserProfile:
    get:
      description: Return user with meta information
      tags:
        - User
      responses:
        200:
          description: User info
          schema:
            $ref: '#/definitions/UserProfileDTO'
        500:
          description: Error
  /getTemplateList:
    get:
      description: Return templates
      tags:
        - Template
      parameters:
        - name: skip
          in: query
          required: true
          type: integer
          format: int32
        - name: count
          in: query
          required: true
          type: integer
          format: int32
        - name: lang
          in: query
          required: true
          type: string
          enum:
            - any
            - english
            - french
        - name: term
          in: query
          type: string
      responses:
        200:
          description: Templates
          schema:
              type: array
              items:
                $ref: '#/definitions/TemplateDTO'
        500:
          description: Error
  /getTemplate:
    get:
      description: Return template by ID
      tags:
        - Template
      parameters:
        - name: id
          in: query
          description: GUID
          required: true
          type: string
      responses:
        200:
          description: Template
          schema:
            $ref: '#/definitions/TemplateDTO'
        500:
          description: Error
  /setupTemplate:
    post:
      description: Setup template
      tags:
        - Template
      parameters:
        - name: template
          in: body
          description: Template that will be setted
          required: true
          schema:
            $ref: '#/definitions/TemplateDTO'
      responses:
        204:
          description: Template setted
        500:
          description: Error
  /admin/getTemplateList:
    get:
      description: Return templates
      tags:
        - Admin template
      parameters:
        - name: skip
          in: query
          required: true
          type: integer
          format: int32
        - name: count
          in: query
          required: true
          type: integer
          format: int32
        - name: lang
          in: query
          required: true
          type: string
          enum:
            - any
            - english
            - french
        - name: status
          in: query
          required: true
          type: string
          enum:
            - any
            - active
            - inactive
        - name: term
          in: query
          type: string
      responses:
        200:
          description: Templates
          schema:
              type: array
              items:
                $ref: '#/definitions/AdminTemplateDTO'
        500:
          description: Error
  /admin/getAllActiveTemplates:
    get:
      description: Return all active templates
      tags:
        - Admin template
      responses:
        200:
          description: Templates
          schema:
            type: array
            items:
              $ref: '#/definitions/TemplateLiteDTO'
        500:
          description: Error
  /admin/getSignatoryTemplate:
    get:
      description: Return template by ID
      tags:
        - Signatory
      parameters:
        - name: id
          in: query
          description: GUID
          required: true
          type: string
        - name: signatoryId
          in: query
          description: Signatory GUID
          required: true
          type: string
      responses:
        200:
          description: Template
          schema:
            $ref: '#/definitions/TemplateDTO'
        500:
          description: Error
  /admin/getTemplate:
    get:
      description: Return template by ID
      tags:
        - Admin template
      parameters:
        - name: id
          in: query
          description: GUID
          required: true
          type: string
      responses:
        200:
          description: Template
          schema:
            $ref: '#/definitions/AdminTemplateDTO'
        500:
          description: Error
  /admin/createTemplate:
    post:
      description: Create new template
      tags:
        - Admin template
      parameters:
        - name: template
          in: body
          description: Template that will be created
          required: true
          schema:
            $ref: '#/definitions/AdminTemplateDTO'
      responses:
        200:
          description: Created template
          schema:
            $ref: '#/definitions/AdminTemplateDTO'
        500:
          description: Error
  /admin/updateTemplate:
    put:
      description: Update existed template
      tags:
        - Admin template
      parameters:
        - name: template
          in: body
          description: Template that will be updated
          required: true
          schema:
            $ref: '#/definitions/AdminTemplateDTO'
      responses:
        200:
          description: Updated template
          schema:
            $ref: '#/definitions/AdminTemplateDTO'
        500:
          description: Error
  /admin/deleteTemplate:
    delete:
      description: Delete template by ID
      tags:
        - Admin template
      parameters:
        - name: id
          in: query
          description: GUID
          required: true
          type: string
      responses:
        204:
          description: Template deleted
        500:
          description: Error
  /admin/login:
    post:
      description: Login
      tags:
        - Admin
      parameters:
        - name: parameters
          in: body
          required: true
          schema:
            $ref: '#/definitions/AdminLoginDTO'
      responses:
        204:
          description: Success
        500:
          description: Error
  /admin/logout:
    post:
      description: Logout
      tags:
        - Admin
      responses:
        204:
          description: Success
        500:
          description: Error
  /admin/resetPassword:
    post:
      description: Password reset
      tags:
        - Admin
      parameters:
        - name: email
          in: body
          description: Email
          required: true
          schema:
            type: string
      responses:
        204:
          description: Success
        500:
          description: Error
  /admin/getCurrentAdminProfile:
    get:
      description: Return admin with meta information
      tags:
        - Admin
      responses:
        200:
          description: Admin info
          schema:
            $ref: '#/definitions/AdminProfileDTO'
        500:
          description: Error
  /admin/getAdminList:
    get:
      description: Return admins
      tags:
        - Admin
      responses:
        200:
          description: Admins
          schema:
            type: array
            items:
              $ref: '#/definitions/AdminDTO'
        500:
          description: Error
  /admin/getAdmin:
    get:
      description: Return admin by ID
      tags:
        - Admin
      parameters:
        - name: id
          in: query
          description: GUID
          required: true
          type: string
      responses:
        200:
          description: Admin
          schema:
            $ref: '#/definitions/AdminDTO'
        500:
          description: Error
  /admin/createAdmin:
    post:
      description: Create new admin
      tags:
        - Admin
      parameters:
        - name: admin
          in: body
          description: Admin that will be created
          required: true
          schema:
            $ref: '#/definitions/AdminCreateDTO'
      responses:
        200:
          description: Created admin
          schema:
            $ref: '#/definitions/AdminEditDTO'
        500:
          description: Error
  /admin/updateAdmin:
    put:
      description: Update existed admin
      tags:
        - Admin
      parameters:
        - name: admin
          in: body
          description: Admin that will be updated
          required: true
          schema:
            $ref: '#/definitions/AdminEditDTO'
      responses:
        200:
          description: Updated admin
          schema:
            $ref: '#/definitions/AdminEditDTO'
        500:
          description: Error
  /admin/deleteAdmin:
    delete:
      description: Delete admin by ID
      tags:
        - Admin
      parameters:
        - name: id
          in: query
          description: GUID
          required: true
          type: string
      responses:
        204:
          description: Admin deleted
        500:
          description: Error
  # Dynamic fields
  /admin/getDynamicFieldList:
    get:
      description: Return fields
      tags:
        - Dynamic field
      responses:
        200:
          description: Fields
          schema:
            $ref: '#/definitions/FieldDTO'
        500:
          description: Error
  /admin/getDynamicField:
    get:
      description: Return field by ID
      tags:
        - Dynamic field
      parameters:
        - name: id
          in: query
          description: GUID
          required: true
          type: string
      responses:
        200:
          description: Field
          schema:
            $ref: '#/definitions/FieldDTO'
        500:
          description: Error
  /admin/createDynamicField:
    post:
      description: Create new field
      tags:
        - Dynamic field
      parameters:
        - name: field
          in: body
          description: Dynamic field that will be created
          required: true
          schema:
            $ref: '#/definitions/FieldDTO'
      responses:
        200:
          description: Created field
          schema:
            $ref: '#/definitions/FieldDTO'
        500:
          description: Error
  /admin/updateDynamicField:
    put:
      description: Update existed field
      tags:
        - Dynamic field
      parameters:
        - name: field
          in: body
          description: Dynamic field that will be updated
          required: true
          schema:
            $ref: '#/definitions/FieldDTO'
      responses:
        200:
          description: Updated field
          schema:
            $ref: '#/definitions/FieldDTO'
        500:
          description: Error
  /admin/deleteDynamicField:
    delete:
      description: Delete field by ID
      tags:
        - Dynamic field
      parameters:
        - name: id
          in: query
          description: GUID
          required: true
          type: string
      responses:
        204:
          description: Dynamic field deleted
        500:
          description: Error
  # Signatories
  /admin/getSignatoryList:
    get:
      description: Return signatories
      tags:
        - Signatory
      parameters:
        - name: skip
          in: query
          required: true
          type: integer
          format: int32
        - name: count
          in: query
          required: true
          type: integer
          format: int32
        - name: term
          in: query
          type: string
      responses:
        200:
          description: Signatories
          schema:
            $ref: '#/definitions/SignatoryDTO'
        500:
          description: Error
  /admin/getSignatory:
    get:
      description: Return signatory by ID
      tags:
        - Signatory
      parameters:
        - name: id
          in: query
          description: GUID
          required: true
          type: string
      responses:
        200:
          description: Signatory
          schema:
            $ref: '#/definitions/SignatoryDTO'
        500:
          description: Error
  /admin/updateSignatory:
    put:
      description: Update existed signatory
      tags:
        - Signatory
      parameters:
        - name: signatory
          in: body
          description: Signatory that will be updated
          required: true
          schema:
            $ref: '#/definitions/SignatoryDTO'
      responses:
        200:
          description: Updated signatory
          schema:
            $ref: '#/definitions/SignatoryDTO'
        500:
          description: Error
  /admin/deleteSignatory:
    delete:
      description: Delete signatory by ID
      tags:
        - Signatory
      parameters:
        - name: id
          in: query
          description: GUID
          required: true
          type: string
      responses:
        204:
          description: Signatory deleted
        500:
          description: Error
  # File
  /admin/files/upload:
    post:
      description: Upload file
      tags:
        - File
      parameters:
        - name: file
          in: formData
          description: File that will be uploaded
          required: true
          type: string
      responses:
        200:
          description: File GUID
        500:
          description: Error
  /admin/files/download:
    get:
      description: Download file by ID
      tags:
        - File
      parameters:
        - name: id
          in: query
          description: File GUID
          required: true
          type: string
      responses:
        200:
          description: File
        500:
          description: Error
definitions:
  UserConnectDTO:
    type: object
    properties:
      email:
        type: string
  UserProfileDTO:
    type: object
    properties:
      email:
        type: string
      templateId:
        type: string
  TemplateDTO:
    type: object
    properties:
      id:
        type: string
      frenchName:
        type: string
      englishName:
        type: string
      lang:
        type: string
        enum:
          - french
          - english
      fields:
        type: array
        uniqueItems: true
        items:
          $ref: '#/definitions/FieldDTO'
      thumb:
        type: string
  TemplateLiteDTO:
    type: object
    properties:
      id:
        type: string
      frenchName:
        type: string
      englishName:
        type: string
  AdminTemplateDTO:
    type: object
    properties:
      id:
        type: string
      frenchName:
        type: string
      englishName:
        type: string
      lang:
        type: string
        enum:
          - french
          - english
      isActive:
        type: boolean
      replacedById:
        type: string
      fieldIds:
        type: array
        uniqueItems: true
        items:
          type: string
      package:
        $ref: '#/definitions/FileDTO'
      thumb:
        $ref: '#/definitions/FileDTO'
  FileDTO:
    type: object
    properties:
      id:
        type: string
      name:
        type: string
  FieldDTO:
    type: object
    properties:
      id:
        type: string
      key:
        type: string
      englishName:
        type: string
      frenchName:
        type: string
      value:
        type: string
  AdminLoginDTO:
    type: object
    properties:
      email:
        type: string
      password:
        type: string
  AdminDTO:
    type: object
    properties:
      id:
        type: string
      email:
        type: string
      lastName:
        type: string
      firstName:
        type: string
      isActive:
        type: boolean
  AdminCreateDTO:
    allOf:
    - type: object
      properties:
        password:
          type: string
    - $ref: "#/definitions/AdminDTO"
  AdminEditDTO:
    allOf:
    - type: object
      properties:
        currentPassword:
          type: string
        newPassword:
          type: string
    - $ref: "#/definitions/AdminDTO"
  AdminProfileDTO:
    type: object
    properties:
      id:
        type: string
      email:
        type: string
      lastName:
        type: string
      firstName:
        type: string
      counters:
        type: object
        properties:
          dynamicFields:
            type: integer
            format: int32
          templates:
            type: integer
            format: int32
          sigantories:
            type: integer
            format: int32
          users:
            type: integer
            format: int32
  SignatoryDTO:
    type: object
    properties:
      id:
        type: string
      email:
        type: string
      lastConnect:
        type: string
        format: date-time
      template:
        $ref: "#/definitions/TemplateDTO"
