const fs = require('fs-extra');
const path = require('path');

fs.copySync(
    path.resolve(__dirname, './ListItem.js'),
    path.resolve(__dirname, '../node_modules/material-ui/List/ListItem.js')
);
